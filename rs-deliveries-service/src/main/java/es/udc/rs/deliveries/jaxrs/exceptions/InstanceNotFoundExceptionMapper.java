package es.udc.rs.deliveries.jaxrs.exceptions;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import es.udc.rs.deliveries.jaxrs.dto.InstanceNotFoundExceptionDtoJaxb;
import es.udc.ws.util.exceptions.InstanceNotFoundException;

@Provider
public class InstanceNotFoundExceptionMapper implements
		ExceptionMapper<InstanceNotFoundException> {

	@Context
	private HttpHeaders headers;

	@Override
	public Response toResponse(InstanceNotFoundException ex) {
		return Response
				.status(Response.Status.NOT_FOUND)
				.entity(new InstanceNotFoundExceptionDtoJaxb(ex.getInstanceId(), ex
						.getInstanceType()))
				.type(headers.getAcceptableMediaTypes().get(0)).build();

	}

}
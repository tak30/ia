package es.udc.rs.deliveries.jaxrs.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import es.udc.rs.deliveries.jaxrs.dto.DeliveryDetailsDtoJaxb;
import es.udc.rs.deliveries.jaxrs.dto.DeliveryDtoJaxb;
import es.udc.rs.deliveries.jaxrs.dto.DeliveryStatusDtoJaxb;
import es.udc.rs.deliveries.model.delivery.Delivery;
import es.udc.rs.deliveries.model.delivery.DeliveryStatus;

public class DeliveryToDeliveryDtoJaxbConversor {

	public static List<DeliveryDetailsDtoJaxb> toDeliveryDetailsDtoJaxb(
			List<Delivery> deliveries) {
		List<DeliveryDetailsDtoJaxb> clientDtos = new ArrayList<>(
				deliveries.size());
		for (int i = 0; i < deliveries.size(); i++) {
			Delivery delivery = deliveries.get(i);
			delivery.setDeliveredDate(null);
			delivery.setDirection("");
			delivery.setPostcode(0);
			delivery.setDeliveryClientId(Long.valueOf(0));
			clientDtos.add(toDeliveryDetailsDtoJaxb(delivery));
		}
		return clientDtos;
	}

	public static DeliveryDetailsDtoJaxb toDeliveryDetailsDtoJaxb(
			Delivery delivery) {
		Calendar estimatedDate = delivery.getCreationDate();
		estimatedDate.set(Calendar.DAY_OF_YEAR, delivery.getEstimatedTime());
		return new DeliveryDetailsDtoJaxb(delivery.getCustomerId(),
				delivery.getDeliveryId(), delivery.getDeliveryClientId(),
				delivery.getCreationDate(), estimatedDate,
				delivery.getDirection(), delivery.getPostcode(),
				delivery.getDeliveredDate(),
				toDeliveryStatusDtoJaxb(delivery.getStatus()));
	}

	public static Delivery toDelivery(DeliveryDetailsDtoJaxb delivery) {
		return new Delivery(delivery.getCustomerId(), delivery.getDeliveryId(),
				delivery.getDeliveryClientId(), delivery.getDirection(),
				delivery.getPostcode());
	}

	public static List<DeliveryDtoJaxb> toDeliveryDtoJaxb(
			List<Delivery> deliveries) {
		List<DeliveryDtoJaxb> clientDtos = new ArrayList<>(deliveries.size());
		for (int i = 0; i < deliveries.size(); i++) {
			Delivery delivery = deliveries.get(i);
			delivery.setDeliveredDate(null);
			delivery.setDirection("");
			delivery.setPostcode(0);
			delivery.setDeliveryClientId(Long.valueOf(0));
			clientDtos.add(toDeliveryDtoJaxb(delivery));
		}
		return clientDtos;
	}

	public static DeliveryDtoJaxb toDeliveryDtoJaxb(Delivery delivery) {
		return new DeliveryDtoJaxb(delivery.getCustomerId(),
				delivery.getDeliveryId(),
				toDeliveryStatusDtoJaxb(delivery.getStatus()));
	}

	private static DeliveryStatusDtoJaxb toDeliveryStatusDtoJaxb(
			DeliveryStatus status) {
		return DeliveryStatusDtoJaxb.valueOf(status.toString());
	}

}

package es.udc.rs.deliveries.jaxrs.resources;

import java.net.URI;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Response.ResponseBuilder;

import es.udc.rs.deliveries.jaxrs.util.CustomerToCustomerDtoJaxbConversor;
import es.udc.rs.deliveries.jaxrs.util.ServiceUtil;
import es.udc.rs.deliveries.jaxrs.dto.CustomerDtoJaxb;
import es.udc.rs.deliveries.jaxrs.dto.CustomerDtoJaxbList;
import es.udc.rs.deliveries.jaxrs.dto.DeliveryDetailsDtoJaxb;
import es.udc.rs.deliveries.jaxrs.dto.DeliveryDtoJaxb;
import es.udc.rs.deliveries.jaxrs.dto.DeliveryDtoJaxbList;
import es.udc.rs.deliveries.jaxrs.util.DeliveryToDeliveryDtoJaxbConversor;
import es.udc.rs.deliveries.model.customer.Customer;
import es.udc.rs.deliveries.model.delivery.Delivery;
import es.udc.rs.deliveries.model.delivery.DeliveryStatus;
import es.udc.rs.deliveries.model.deliveryservice.DeliveryService;
import es.udc.rs.deliveries.model.deliveryservice.DeliveryServiceFactory;
import es.udc.rs.deliveries.util.exceptions.InstanceNotCancellableException;
import es.udc.rs.deliveries.util.exceptions.InstanceNotRemovableException;
import es.udc.rs.deliveries.util.exceptions.StatusNotValidException;
import es.udc.ws.util.exceptions.InputValidationException;
import es.udc.ws.util.exceptions.InstanceNotFoundException;

@Path("/deliveries")
public class DeliveryResource {

	@POST
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Response addClient(DeliveryDetailsDtoJaxb deliveryDto, @Context UriInfo ui,
			@Context HttpHeaders headers) throws InputValidationException,
			InstanceNotFoundException {
		DeliveryService service = DeliveryServiceFactory.getService();
		Delivery deliveryAdded = service
				.addDelivery(DeliveryToDeliveryDtoJaxbConversor
						.toDelivery(deliveryDto));
		DeliveryDtoJaxb resultDeliveryDto = DeliveryToDeliveryDtoJaxbConversor
				.toDeliveryDtoJaxb(deliveryAdded);
		return Response
				.created(
						URI.create(ui.getRequestUri().toString() + "/"
								+ resultDeliveryDto.getDeliveryId()))
				.entity(resultDeliveryDto)
				.type(headers.getAcceptableMediaTypes().get(0)).build();
	}

	@GET
	@Path("/{id}")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public DeliveryDetailsDtoJaxb findById(@PathParam("id") String id)
			throws InstanceNotFoundException, InputValidationException {
		Long deliveryId;
		try {
			deliveryId = Long.valueOf(id);
		} catch (NumberFormatException ex) {
			throw new InputValidationException("Invalid Request: "
					+ "unable to parse customer id '" + id + "'");
		}
		DeliveryService service = DeliveryServiceFactory.getService();
		Delivery deliveryFound = service.findDelivery(deliveryId);
		return DeliveryToDeliveryDtoJaxbConversor
				.toDeliveryDetailsDtoJaxb(deliveryFound);
	}

	@POST
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Path("/{id}/status/")
	public void updateDeliveryStatus(@PathParam("id") String id,
			@QueryParam("status") String status)
			throws InstanceNotFoundException, StatusNotValidException, InputValidationException {
		Long deliveryId;
		try {
			deliveryId = Long.valueOf(id);
		} catch (NumberFormatException ex) {
			throw new InputValidationException("Invalid Request: "
					+ "unable to parse customer id '" + id + "'");
		}
		DeliveryStatus statusEnum = DeliveryStatus.valueOf(status);
		DeliveryServiceFactory.getService().updateDeliveryStatus(
				deliveryId, statusEnum);
	}

	@POST
	@Path("/{id}/cancel")
	public void cancelDelivery(@PathParam("id") String id)
			throws InstanceNotFoundException, InstanceNotCancellableException, InputValidationException {

		Long deliveryId;
		try {
			deliveryId = Long.valueOf(id);
		} catch (NumberFormatException ex) {
			throw new InputValidationException("Invalid Request: "
					+ "unable to parse customer id '" + id + "'");
		}

		DeliveryServiceFactory.getService().cancelDelivery(deliveryId);
	}

	@GET
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Response findDeliveries(
			@DefaultValue("") @QueryParam("id") String customerId,
			@DefaultValue("") @QueryParam("status") String status,
			@DefaultValue("0") @QueryParam("startIndex") int startIndex,
			@DefaultValue("2") @QueryParam("count") int count,
			@Context UriInfo uriInfo, @Context HttpHeaders headers)
			throws InstanceNotFoundException {

		Long customerIdLong = null;
		if (!customerId.isEmpty()) {
			customerIdLong = Long.valueOf(customerId).longValue();
		}
		DeliveryStatus statusEnum = null;
		if (!status.isEmpty()) {
			statusEnum = DeliveryStatus.valueOf(status);
		}
		DeliveryService service = DeliveryServiceFactory.getService();
		List<Delivery> deliveries = service.findDeliveries(customerIdLong,
				statusEnum, startIndex, count);

		String type = ServiceUtil.getTypeAsStringFromHeaders(headers);

		List<DeliveryDtoJaxb> deliveryDtos = DeliveryToDeliveryDtoJaxbConversor
				.toDeliveryDtoJaxb(deliveries);

		Link selfLink = getSelfLink(uriInfo, customerId, status, startIndex,
				count, type);
		Link nextLink = getNextLink(uriInfo, customerId, status, startIndex,
				count, deliveries.size(), type);
		Link previousLink = getPreviousLink(uriInfo, customerId, status,
				startIndex, count, type);
		ResponseBuilder response = Response.ok(
				new DeliveryDtoJaxbList(deliveryDtos)).links(selfLink);
		if (nextLink != null) {
			response.links(nextLink);
		}
		if (previousLink != null) {
			response.links(previousLink);
		}
		return response.build();
	}

	private static Link getNextLink(UriInfo uriInfo, String customerId,
			String status, int startIndex, int count, int numberOfProducts,
			String type) {
		if (numberOfProducts < count) {
			return null;
		}
		return ServiceUtil.getDeliveriesIntervalLink(uriInfo, customerId,
				status, startIndex + count, count, "next",
				"Next interval of deliveries", type);
	}

	private Link getPreviousLink(UriInfo uriInfo, String customerId,
			String status, int startIndex, int count, String type) {
		if (startIndex <= 0) {
			return null;
		}
		startIndex = startIndex - count;
		if (startIndex < 0) {
			startIndex = 0;
		}
		return ServiceUtil.getDeliveriesIntervalLink(uriInfo, customerId,
				status, startIndex, count, "previous",
				"Previous interval of deliveries", type);
	}

	private Link getSelfLink(UriInfo uriInfo, String customerId, String status,
			int startIndex, int count, String type) {
		return ServiceUtil.getDeliveriesIntervalLink(uriInfo, customerId,
				status, startIndex, count, "self",
				"Current interval of deliveries", type);
	}
}

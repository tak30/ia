package es.udc.rs.deliveries.jaxrs.resources;

import java.net.URI;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Response.ResponseBuilder;

import es.udc.rs.deliveries.jaxrs.util.ServiceUtil;
import es.udc.rs.deliveries.jaxrs.dto.CustomerDetailsDtoJaxb;
import es.udc.rs.deliveries.jaxrs.dto.CustomerDtoJaxb;
import es.udc.rs.deliveries.jaxrs.dto.CustomerDtoJaxbList;
import es.udc.rs.deliveries.jaxrs.util.CustomerToCustomerDtoJaxbConversor;
import es.udc.rs.deliveries.model.customer.Customer;
import es.udc.rs.deliveries.model.deliveryservice.DeliveryService;
import es.udc.rs.deliveries.model.deliveryservice.DeliveryServiceFactory;
import es.udc.rs.deliveries.util.exceptions.InstanceNotRemovableException;
import es.udc.ws.util.exceptions.InputValidationException;
import es.udc.ws.util.exceptions.InstanceNotFoundException;

@Path("/customers")
public class CustomerResource {

	@POST
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Response addClient(CustomerDetailsDtoJaxb customerDto, @Context UriInfo ui,
			@Context HttpHeaders headers) throws InputValidationException {
		DeliveryService service = DeliveryServiceFactory.getService();
		Customer customerAdded = service
				.addCustomer(CustomerToCustomerDtoJaxbConversor
						.toCustomer(customerDto));
		CustomerDtoJaxb resultCustomerDto = CustomerToCustomerDtoJaxbConversor
				.toCustomerDtoJaxb(customerAdded);
		return Response
				.created(
						URI.create(ui.getRequestUri().toString() + "/"
								+ resultCustomerDto.getCustomerId()))
				.entity(resultCustomerDto)
				.type(headers.getAcceptableMediaTypes().get(0)).build();
	}

	@GET
	@Path("/{id}")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public CustomerDetailsDtoJaxb findById(@PathParam("id") String id)
			throws  InstanceNotFoundException, InputValidationException {
		
		Long customerId;
		try {
			customerId = Long.valueOf(id);
		} catch (NumberFormatException ex) {
			throw new InputValidationException("Invalid Request: "
					+ "unable to parse customer id '" + id + "'");
		}
		
		DeliveryService service = DeliveryServiceFactory.getService();
		Customer customerFound = service.findCustomer(customerId);
		return CustomerToCustomerDtoJaxbConversor
				.toCustomerDetailsDtoJaxb(customerFound);
	}
	
	@PUT
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Path("/{id}")
	public void updateCustomer(CustomerDetailsDtoJaxb customerDto, @PathParam("id") String id)
			throws InputValidationException, InstanceNotFoundException {

		Long customerId;
		try {
			customerId = Long.valueOf(id);
		} catch (NumberFormatException ex) {
			throw new InputValidationException("Invalid Request: "
					+ "unable to parse customer id '" + id + "'");
		}

		Customer customer = CustomerToCustomerDtoJaxbConversor.toCustomer(customerDto);
		customer.setCustomerId(customerId);
		DeliveryServiceFactory.getService().updateCustomer(customer);

	}
	
	@DELETE
	@Path("/{id}")
	public void deleteCustomer(@PathParam("id") String id)
			throws InstanceNotFoundException, InstanceNotRemovableException, InputValidationException {

		Long customerId;
		try {
			customerId = Long.valueOf(id);
		} catch (NumberFormatException ex) {
			throw new InputValidationException("Invalid Request: "
					+ "unable to parse customer id '" + id + "'");
		}	
		DeliveryServiceFactory.getService().deleteCustomer(customerId);
	}

	@GET
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Response findCustomersByKeyword(
			@DefaultValue("") @QueryParam("keyword") String keyword,
			@DefaultValue("1") @QueryParam("startIndex") int startIndex,
			@DefaultValue("2") @QueryParam("count") int count,
			@Context UriInfo uriInfo, @Context HttpHeaders headers) {

		DeliveryService service = DeliveryServiceFactory.getService();
		List<Customer> customers = service.findCustomerByKeywords(keyword,
				startIndex, count);
		
		String type = ServiceUtil.getTypeAsStringFromHeaders(headers);
		
		List<CustomerDtoJaxb> customerDtos = CustomerToCustomerDtoJaxbConversor
				.toCustomerDtoJaxb(customers);

		Link selfLink = getSelfLink(uriInfo, keyword, startIndex, count, type);
		Link nextLink = getNextLink(uriInfo, keyword, startIndex, count,
				customers.size(), type);
		Link previousLink = getPreviousLink(uriInfo, keyword, startIndex,
				count, type);
		ResponseBuilder response = Response.ok(
				new CustomerDtoJaxbList(customerDtos)).links(selfLink);
		if (nextLink != null) {
			response.links(nextLink);
		}
		if (previousLink != null) {
			response.links(previousLink);
		}
		return response.build();
	}
	
	

	
	private static Link getNextLink(UriInfo uriInfo, String keyword,
			int startIndex, int count, int numberOfProducts, String type) {
		if (numberOfProducts < count) {
			return null;
		}
		return ServiceUtil.getCustomersIntervalLink(uriInfo, keyword, startIndex
				+ count, count, "next", "Next interval of customers", type);
	}

	private Link getPreviousLink(UriInfo uriInfo, String keyword,
			int startIndex, int count, String type) {
		if (startIndex <= 0) {
			return null;
		}
		startIndex = startIndex - count;
		if (startIndex < 0) {
			startIndex = 0;
		}
		return ServiceUtil.getCustomersIntervalLink(uriInfo, keyword,
				startIndex, count, "previous", "Previous interval of customers",
				type);
	}

	private Link getSelfLink(UriInfo uriInfo, String keyword, int startIndex,
			int count, String type) {
		return ServiceUtil
				.getCustomersIntervalLink(uriInfo, keyword, startIndex, count,
						"self", "Current interval of customers", type);
	}
}

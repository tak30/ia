package es.udc.rs.deliveries.jaxrs.exceptions;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import es.udc.rs.deliveries.jaxrs.dto.InstanceNotCancellableExceptionDtoJaxb;
import es.udc.rs.deliveries.util.exceptions.InstanceNotCancellableException;


@Provider
public class InstanceNotCancellableExceptionMapper implements
		ExceptionMapper<InstanceNotCancellableException> {

	@Context
	private HttpHeaders headers;

	@Override
	public Response toResponse(InstanceNotCancellableException ex) {
		return Response
				.status(Response.Status.GONE)
				.entity(new InstanceNotCancellableExceptionDtoJaxb(ex.getInstanceId(), ex
						.getInstanceType()))
				.type(headers.getAcceptableMediaTypes().get(0)).build();

	}

}
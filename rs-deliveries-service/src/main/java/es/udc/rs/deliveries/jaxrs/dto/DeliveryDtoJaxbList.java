package es.udc.rs.deliveries.jaxrs.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "deliveries")
@XmlType(name = "deliveryListType")
public class DeliveryDtoJaxbList {

	@XmlElement(name = "delivery")
	private List<DeliveryDtoJaxb> deliveries = null;

	public DeliveryDtoJaxbList() {
		this.deliveries = new ArrayList<DeliveryDtoJaxb>();
	}

	public DeliveryDtoJaxbList(List<DeliveryDtoJaxb> deliveries) {
		this.deliveries = deliveries;
	}

	public List<DeliveryDtoJaxb> getDeliveries() {
		return deliveries;
	}

	public void setDeliveries(List<DeliveryDtoJaxb> deliveries) {
		this.deliveries = deliveries;
	}

}

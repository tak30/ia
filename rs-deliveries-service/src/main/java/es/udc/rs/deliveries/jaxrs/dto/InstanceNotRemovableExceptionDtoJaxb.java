package es.udc.rs.deliveries.jaxrs.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "InstanceNotRemovableException")
@XmlType(name="instanceNotRemovableExceptionType", propOrder = {"instanceId", "instanceType"})
public class InstanceNotRemovableExceptionDtoJaxb {

	@XmlElement(name="instance-id", required = true)
	private String instanceId;
	@XmlElement(name="instance-type", required = true)
	private String instanceType;

	public InstanceNotRemovableExceptionDtoJaxb() {
	}

	public InstanceNotRemovableExceptionDtoJaxb(Object instanceId, String instanceType) {
		this.instanceId = instanceId.toString();
		this.instanceType = instanceType;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public String getInstanceType() {
		return instanceType;
	}

	public void setInstanceType(String instanceType) {
		this.instanceType = instanceType;
	}


}

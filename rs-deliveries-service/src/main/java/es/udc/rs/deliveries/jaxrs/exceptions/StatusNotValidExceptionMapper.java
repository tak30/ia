package es.udc.rs.deliveries.jaxrs.exceptions;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import es.udc.rs.deliveries.jaxrs.dto.InputValidationExceptionDtoJaxb;
import es.udc.rs.deliveries.jaxrs.dto.InstanceNotRemovableExceptionDtoJaxb;
import es.udc.rs.deliveries.jaxrs.dto.StatusNotValidExceptionDtoJaxb;
import es.udc.rs.deliveries.util.exceptions.InstanceNotRemovableException;
import es.udc.rs.deliveries.util.exceptions.StatusNotValidException;
import es.udc.ws.util.exceptions.InputValidationException;

@Provider
public class StatusNotValidExceptionMapper implements
		ExceptionMapper<StatusNotValidException> {

	@Context
	private HttpHeaders headers;

	@Override
	public Response toResponse(StatusNotValidException ex) {
		return Response.status(Response.Status.CONFLICT)
				.entity(new StatusNotValidExceptionDtoJaxb(ex.getMessage()))
				.type(headers.getAcceptableMediaTypes().get(0)).build();
	}

}
package es.udc.rs.deliveries.jaxrs.exceptions;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import es.udc.rs.deliveries.jaxrs.dto.InputValidationExceptionDtoJaxb;
import es.udc.ws.util.exceptions.InputValidationException;

@Provider
public class InputValidationExceptionMapper implements
		ExceptionMapper<InputValidationException> {

	@Context
	private HttpHeaders headers;

	@Override
	public Response toResponse(InputValidationException ex) {
		return Response.status(Response.Status.BAD_REQUEST)
				.entity(new InputValidationExceptionDtoJaxb(ex.getMessage()))
				.type(headers.getAcceptableMediaTypes().get(0)).build();

	}

}

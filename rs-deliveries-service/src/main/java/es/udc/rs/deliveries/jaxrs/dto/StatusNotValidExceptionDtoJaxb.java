package es.udc.rs.deliveries.jaxrs.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name="StatusNotValidException")
@XmlType(name="statusNotValidExceptionType")
public class StatusNotValidExceptionDtoJaxb {

    @XmlElement(required = true)
	private String message;

    public StatusNotValidExceptionDtoJaxb() {
    }

    public StatusNotValidExceptionDtoJaxb(String message) {
    	this.message = message;
    }

    public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


}

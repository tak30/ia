package es.udc.rs.deliveries.jaxrs.util;

import java.util.ArrayList;
import java.util.List;

import es.udc.rs.deliveries.jaxrs.dto.CustomerDetailsDtoJaxb;
import es.udc.rs.deliveries.jaxrs.dto.CustomerDtoJaxb;
import es.udc.rs.deliveries.model.customer.Customer;

public class CustomerToCustomerDtoJaxbConversor {

	public static List<CustomerDetailsDtoJaxb> toCustomerDetailsDtoJaxb(
			List<Customer> customers) {
		List<CustomerDetailsDtoJaxb> customerDtos = new ArrayList<>(
				customers.size());
		for (int i = 0; i < customers.size(); i++) {
			Customer customer = customers.get(i);
			customer.setCif("");
			customer.setDirection("");
			customer.setPostcode(0);
			customerDtos.add(toCustomerDetailsDtoJaxb(customer));
		}
		return customerDtos;
	}

	public static CustomerDetailsDtoJaxb toCustomerDetailsDtoJaxb(
			Customer customer) {
		return new CustomerDetailsDtoJaxb(customer.getCustomerId(),
				customer.getName(), customer.getCif(), customer.getDirection(),
				customer.getPostcode());
	}

	public static Customer toCustomer(CustomerDetailsDtoJaxb customer) {
		return new Customer(customer.getName(), customer.getCif(),
				customer.getDirection(), customer.getPostcode());
	}

	public static List<CustomerDtoJaxb> toCustomerDtoJaxb(
			List<Customer> customers) {
		List<CustomerDtoJaxb> clientDtos = new ArrayList<>(customers.size());
		for (int i = 0; i < customers.size(); i++) {
			Customer customer = customers.get(i);
			customer.setCif("");
			customer.setDirection("");
			customer.setPostcode(0);
			clientDtos.add(toCustomerDtoJaxb(customer));
		}
		return clientDtos;
	}

	public static CustomerDtoJaxb toCustomerDtoJaxb(Customer customer) {
		return new CustomerDtoJaxb(customer.getCustomerId(), customer.getName());
	}

}

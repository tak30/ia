package es.udc.rs.deliveries.jaxrs.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "customers")
@XmlType(name = "customerListType")
public class CustomerDtoJaxbList {

	@XmlElement(name = "customer")
	private List<CustomerDtoJaxb> customers = null;

	public CustomerDtoJaxbList() {
		this.customers = new ArrayList<CustomerDtoJaxb>();
	}

	public CustomerDtoJaxbList(List<CustomerDtoJaxb> customers) {
		this.customers = customers;
	}

	public List<CustomerDtoJaxb> getCustomers() {
		return customers;
	}

	public void setCustomers(List<CustomerDtoJaxb> customers) {
		this.customers = customers;
	}

}

package es.udc.rs.deliveries.jaxrs.exceptions;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import es.udc.rs.deliveries.jaxrs.dto.InstanceNotRemovableExceptionDtoJaxb;
import es.udc.rs.deliveries.util.exceptions.InstanceNotRemovableException;

@Provider
public class InstanceNotRemovableExceptionMapper implements
		ExceptionMapper<InstanceNotRemovableException> {

	@Context
	private HttpHeaders headers;

	@Override
	public Response toResponse(InstanceNotRemovableException ex) {
		return Response
				.status(Response.Status.FORBIDDEN)
				.entity(new InstanceNotRemovableExceptionDtoJaxb(ex.getInstanceId(), ex
						.getInstanceType()))
				.type(headers.getAcceptableMediaTypes().get(0)).build();

	}

}
package es.udc.rs.deliveries.jaxrs.dto;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "deliveryStatusType")
@XmlEnum
public enum DeliveryStatusDtoJaxb {

	@XmlEnumValue("PENDING")
	PENDING("PENDING"), @XmlEnumValue("SENT")
	SENT("SENT"), @XmlEnumValue("DELIVERED")
	DELIVERED("DELIVERED"), @XmlEnumValue("REJECTED")
	REJECTED("REJECTED"), @XmlEnumValue("CANCELLED")
	CANCELLED("CANCELLED");

	private final String value;

	DeliveryStatusDtoJaxb(String v) {
		value = v;
	}

	public String value() {
		return value;
	}

	public static DeliveryStatusDtoJaxb fromValue(String v) {
		for (DeliveryStatusDtoJaxb c : DeliveryStatusDtoJaxb.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

}

package es.udc.rs.deliveries.util.exceptions;

@SuppressWarnings("serial")
public class StatusNotValidException extends Exception {

    public StatusNotValidException(String message) {
        super(message);
    }
}

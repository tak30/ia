package es.udc.rs.deliveries.util.exceptions;

@SuppressWarnings("serial")
public class InstanceNotRemovableException extends Exception {

    private Object instanceId;
    private String instanceType;

    public InstanceNotRemovableException(Object instanceId, String instanceType) {

        super("Instance not removable (identifier = '" + instanceId + "' - type = '"
                + instanceType + "')");
        this.instanceId = instanceId;
        this.instanceType = instanceType;

    }

    public Object getInstanceId() {
        return instanceId;
    }

    public String getInstanceType() {
        return instanceType;
    }
}
package es.udc.rs.deliveries.util.exceptions;

@SuppressWarnings("serial")
public class InstanceNotCancellableException extends Exception {

    private Object instanceId;
    private String instanceType;

    public InstanceNotCancellableException(Object instanceId, String instanceType) {

        super("Instance not cancellable (identifier = '" + instanceId + "' - type = '"
                + instanceType + "')");
        this.instanceId = instanceId;
        this.instanceType = instanceType;

    }

    public Object getInstanceId() {
        return instanceId;
    }

    public String getInstanceType() {
        return instanceType;
    }
}
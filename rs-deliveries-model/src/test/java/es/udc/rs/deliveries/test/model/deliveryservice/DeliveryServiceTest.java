package es.udc.rs.deliveries.test.model.deliveryservice;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import es.udc.rs.deliveries.model.customer.Customer;
import es.udc.rs.deliveries.model.delivery.Delivery;
import es.udc.rs.deliveries.model.delivery.DeliveryStatus;
import es.udc.rs.deliveries.model.deliveryservice.DeliveryService;
import es.udc.rs.deliveries.model.deliveryservice.DeliveryServiceFactory;
import es.udc.rs.deliveries.model.deliveryservice.MockDeliveryService;
import es.udc.rs.deliveries.util.exceptions.InstanceNotCancellableException;
import es.udc.rs.deliveries.util.exceptions.InstanceNotRemovableException;
import es.udc.rs.deliveries.util.exceptions.StatusNotValidException;
import es.udc.ws.util.exceptions.InputValidationException;
import es.udc.ws.util.exceptions.InstanceNotFoundException;
import es.udc.ws.util.sql.DataSourceLocator;
import es.udc.ws.util.sql.SimpleDataSource;

public class DeliveryServiceTest {

	private final long NON_EXISTENT_DELIVERY_ID = -1;
	private final long NON_EXISTENT_CUSTOMER_ID = -1;
	private final long NON_EXISTENT_DELIVERY_CLIENT_ID = -1;
	private final String USER_ID = "rs-user";

	private static DeliveryService deliveryService = null;

	private Customer getValidCustomer(String name) {
		return new Customer(name, "45679232V",
				"Calle de la Uva, 4", new Integer(14434));
	}

	private Delivery getValidDelivery(Long customerId) {
		return new Delivery(customerId, new Long(1337), "Avenida de Yanez, 34",
				new Integer(1234));
	}

	@Before
	public void init() {
		deliveryService = new MockDeliveryService();
	}

	@Test
	public void testAddCustomerAndFindCustomer()
			throws InputValidationException, InstanceNotFoundException {
		Customer customer = getValidCustomer("Pixar");
		Customer addedCustomer = null;
		addedCustomer = deliveryService.addCustomer(customer);
		Customer foundCustomer = deliveryService.findCustomer(addedCustomer
				.getCustomerId());
		assertEquals(addedCustomer, foundCustomer);
	}
	
	@Test
	public void testFindCustomerByKeywords()
			throws InputValidationException, InstanceNotFoundException {
		Customer customer1 = getValidCustomer("Pixar");
		Customer addedCustomer1 = deliveryService.addCustomer(customer1);
		Customer customer2 = getValidCustomer("Pixar");
		Customer addedCustomer2 = deliveryService.addCustomer(customer2);
		Customer customer3 = getValidCustomer("Pixar");
		Customer addedCustomer3 = deliveryService.addCustomer(customer3);
		List<Customer> foundCustomers = deliveryService.findCustomerByKeywords("Pixar", -1, -1);
		assertEquals(3, foundCustomers.size());
		foundCustomers = deliveryService.findCustomerByKeywords("Pixar", 1, 2);
		assertEquals(2, foundCustomers.size());
		assertEquals(addedCustomer1, foundCustomers.get(0));
		assertEquals(addedCustomer2, foundCustomers.get(1));		
		foundCustomers = deliveryService.findCustomerByKeywords("Pixar", 1, 4);
		assertEquals(3, foundCustomers.size());
		foundCustomers = deliveryService.findCustomerByKeywords("Pixar", 2, 4);
		assertEquals(2, foundCustomers.size());

	}

	@Test
	public void testAddInvalidCustomer() {

		Customer customer = getValidCustomer("Pixar");
		Customer addedCustomer = null;
		boolean exceptionCatched = false;

		// Check customer name not null
		customer.setName(null);
		try {
			addedCustomer = deliveryService.addCustomer(customer);
		} catch (InputValidationException e) {
			exceptionCatched = true;
		}
		assertTrue(exceptionCatched);

		// Check customer name not empty
		exceptionCatched = false;
		customer = getValidCustomer("Pixar");
		customer.setName("");
		try {
			addedCustomer = deliveryService.addCustomer(customer);
		} catch (InputValidationException e) {
			exceptionCatched = true;
		}
		assertTrue(exceptionCatched);


		// Check customer cif not null
		exceptionCatched = false;
		customer = getValidCustomer("Pixar");
		customer.setCif(null);
		try {
			addedCustomer = deliveryService.addCustomer(customer);
		} catch (InputValidationException e) {
			exceptionCatched = true;
		}
		assertTrue(exceptionCatched);

		// Check customer cif not empty
		exceptionCatched = false;
		customer = getValidCustomer("Pixar");
		customer.setCif("");
		try {
			addedCustomer = deliveryService.addCustomer(customer);
		} catch (InputValidationException e) {
			exceptionCatched = true;
		}
		assertTrue(exceptionCatched);

		// Check customer direction not null
		exceptionCatched = false;
		customer = getValidCustomer("Pixar");
		customer.setDirection(null);
		try {
			addedCustomer = deliveryService.addCustomer(customer);
		} catch (InputValidationException e) {
			exceptionCatched = true;
		}
		assertTrue(exceptionCatched);

		// Check customer direction not empty
		exceptionCatched = false;
		customer = getValidCustomer("Pixar");
		customer.setDirection("");
		try {
			addedCustomer = deliveryService.addCustomer(customer);
		} catch (InputValidationException e) {
			exceptionCatched = true;
		}
		assertTrue(exceptionCatched);

	}

	@Test(expected = InstanceNotFoundException.class)
	public void testFindNonExistentCustomer() throws InstanceNotFoundException {
		deliveryService.findCustomer(NON_EXISTENT_CUSTOMER_ID);
	}

	@Test
	public void testUpdateCustomer() throws InputValidationException,
			InstanceNotFoundException {

		Customer customer = deliveryService.addCustomer(getValidCustomer(
				"Pixar"));

		customer.setName("new name");
		customer.setCif("new cif");
		customer.setDirection("new direction");

		deliveryService.updateCustomer(customer);

		Customer updatedCustomer = deliveryService.findCustomer(customer
				.getCustomerId());
		assertEquals(customer, updatedCustomer);

	}

	@Test(expected = InputValidationException.class)
	public void testUpdateInvalidCustomer() throws InputValidationException,
			InstanceNotFoundException {

		Customer customer = deliveryService.addCustomer(getValidCustomer(
				"Pixar"));
		// Check customer title not null
		customer = deliveryService.findCustomer(customer.getCustomerId());
		customer.setCif("");
		deliveryService.updateCustomer(customer);

	}

	@Test(expected = InstanceNotFoundException.class)
	public void testUpdateNonExistentCustomer()
			throws InputValidationException, InstanceNotFoundException {

		Customer customer = getValidCustomer("Pixar");
		customer.setCustomerId(NON_EXISTENT_CUSTOMER_ID);
		deliveryService.updateCustomer(customer);
	}

	@Test(expected = InstanceNotFoundException.class)
	public void testRemoveCustomer() throws InstanceNotFoundException,
			InputValidationException {

		Customer customer = deliveryService.addCustomer(getValidCustomer(
				"Pixar"));
		boolean exceptionCatched = false;
		try {
			deliveryService.deleteCustomer(customer.getCustomerId());
		} catch (InstanceNotFoundException | InstanceNotRemovableException e) {
			exceptionCatched = true;
		}
		assertTrue(!exceptionCatched);
		deliveryService.findCustomer(customer.getCustomerId());
	}

	@Test(expected = InstanceNotFoundException.class)
	public void testRemoveNonExistentCustomer()
			throws InstanceNotFoundException {
		try {
			deliveryService.deleteCustomer(NON_EXISTENT_CUSTOMER_ID);
		} catch (InstanceNotRemovableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// *******************DELIVERIES TESTS*****************************

	@Test
	public void testAddDeliveryAndFindDelivery()
			throws InputValidationException, InstanceNotFoundException {
		Customer customer = getValidCustomer("Pixar");
		Customer addedCustomer = deliveryService.addCustomer(customer);
		Delivery delivery = getValidDelivery(addedCustomer.getCustomerId());
		Delivery addedDelivery = deliveryService.addDelivery(delivery);
		Delivery foundDelivery = deliveryService.findDelivery(addedDelivery
				.getDeliveryId());
		assertEquals(addedDelivery, foundDelivery);
	}

	@Test(expected = InputValidationException.class)
	public void testAddInvalidDelivery() throws InstanceNotFoundException,
			InputValidationException {

		Customer customer = getValidCustomer("Ares");
		customer = deliveryService.addCustomer(customer);
		Delivery delivery = getValidDelivery(customer.getCustomerId());

		// Check delivery address not null
		delivery.setDirection(null);
		Delivery generatedDelivery = deliveryService.addDelivery(delivery);

	}

	@Test(expected = InstanceNotFoundException.class)
	public void TestAddDeliveryWithNonExistentCustomer()
			throws InstanceNotFoundException, InputValidationException {

		Delivery delivery = getValidDelivery(NON_EXISTENT_CUSTOMER_ID);
		deliveryService.addDelivery(delivery);

	}

	@Test(expected = InstanceNotFoundException.class)
	public void TestFindNotExistentDelivery() throws InstanceNotFoundException {
		Delivery foundDelivery = deliveryService
				.findDelivery(NON_EXISTENT_DELIVERY_ID);
	}

	@Test
	public void TestFindDeliveries() throws InstanceNotFoundException,
			StatusNotValidException, InputValidationException {

		Customer customer1 = getValidCustomer("Ares");
		Customer customer2 = getValidCustomer("Ares");
		Customer customer3 = getValidCustomer("Azure");

		customer1 = deliveryService.addCustomer(customer1);
		customer2 = deliveryService.addCustomer(customer2);
		customer3 = deliveryService.addCustomer(customer3);

		Delivery delivery = getValidDelivery(customer1.getCustomerId());
		Delivery delivery2 = getValidDelivery(customer2.getCustomerId());
		Delivery delivery3 = getValidDelivery(customer3.getCustomerId());
		Delivery delivery4 = getValidDelivery(customer1.getCustomerId());
		Delivery delivery5 = getValidDelivery(customer1.getCustomerId());
		Delivery delivery6 = getValidDelivery(customer3.getCustomerId());
		Delivery delivery7 = getValidDelivery(customer2.getCustomerId());

		delivery = deliveryService.addDelivery(delivery);
		delivery2 = deliveryService.addDelivery(delivery2);
		delivery3 = deliveryService.addDelivery(delivery3);
		delivery4 = deliveryService.addDelivery(delivery4);
		delivery5 = deliveryService.addDelivery(delivery5);
		delivery6 = deliveryService.addDelivery(delivery6);
		delivery7 = deliveryService.addDelivery(delivery7);

		List<Delivery> foundDeliveries = deliveryService.findDeliveries(
				customer1.getCustomerId(), null, -1, -1);
		assertEquals(3, foundDeliveries.size());
		assertEquals(delivery, foundDeliveries.get(0));
		assertEquals(delivery4, foundDeliveries.get(1));
		assertEquals(delivery5, foundDeliveries.get(2));
		
		foundDeliveries = deliveryService.findDeliveries(
				customer1.getCustomerId(), null, 2, 2);
		assertEquals(2, foundDeliveries.size());
		assertEquals(delivery4, foundDeliveries.get(0));
		assertEquals(delivery5, foundDeliveries.get(1));

		foundDeliveries = deliveryService.findDeliveries(
				customer2.getCustomerId(), null, -1, -1);
		assertEquals(2, foundDeliveries.size());

		foundDeliveries = deliveryService.findDeliveries(
				customer3.getCustomerId(), null, -1, -1);
		assertEquals(2, foundDeliveries.size());

		// Status -> pending, sent, delivered, rejected, cancelled
		deliveryService.updateDeliveryStatus(delivery2.getDeliveryId(),
				DeliveryStatus.SENT);
		deliveryService.updateDeliveryStatus(delivery2.getDeliveryId(),
				DeliveryStatus.REJECTED);
		deliveryService.updateDeliveryStatus(delivery3.getDeliveryId(),
				DeliveryStatus.SENT);
		deliveryService.updateDeliveryStatus(delivery4.getDeliveryId(),
				DeliveryStatus.SENT);
		deliveryService.updateDeliveryStatus(delivery5.getDeliveryId(),
				DeliveryStatus.SENT);
		deliveryService.updateDeliveryStatus(delivery6.getDeliveryId(),
				DeliveryStatus.SENT);
		deliveryService.updateDeliveryStatus(delivery7.getDeliveryId(),
				DeliveryStatus.SENT);
		deliveryService.updateDeliveryStatus(delivery6.getDeliveryId(),
				DeliveryStatus.DELIVERED);
		deliveryService.updateDeliveryStatus(delivery7.getDeliveryId(),
				DeliveryStatus.DELIVERED);

		foundDeliveries = deliveryService.findDeliveries(null,
				DeliveryStatus.SENT, -1, -1);
		assertEquals(3, foundDeliveries.size());

		foundDeliveries = deliveryService.findDeliveries(null,
				DeliveryStatus.DELIVERED, -1, -1);
		assertEquals(2, foundDeliveries.size());

		// both
		foundDeliveries = deliveryService.findDeliveries(
				customer1.getCustomerId(), DeliveryStatus.SENT, -1, -1);
		assertEquals(2, foundDeliveries.size());

		foundDeliveries = deliveryService.findDeliveries(
				customer3.getCustomerId(), DeliveryStatus.DELIVERED, -1, -1);
		assertEquals(1, foundDeliveries.size());

	}

	public void TestFindDeliveriesNonExistentCustomer()
			throws InstanceNotFoundException {

		List<Delivery> foundDeliveries = deliveryService.findDeliveries(
				NON_EXISTENT_CUSTOMER_ID, null, -1, -1);
	}

	@Test
	public void TestCancelDelivery() throws InstanceNotFoundException,
			InputValidationException, InstanceNotCancellableException {

		Customer customer1 = getValidCustomer("Ares");
		customer1 = deliveryService.addCustomer(customer1);
		Delivery delivery = getValidDelivery(customer1.getCustomerId());
		delivery = deliveryService.addDelivery(delivery);

		assertEquals(DeliveryStatus.PENDING, delivery.getStatus());

		deliveryService.cancelDelivery(delivery.getDeliveryId());
		assertEquals(DeliveryStatus.CANCELLED, delivery.getStatus());
	}

	@Test(expected = InstanceNotFoundException.class)
	public void TestDeleteNotExistentDelivery()
			throws InstanceNotFoundException, InstanceNotCancellableException {

		deliveryService.cancelDelivery(NON_EXISTENT_DELIVERY_ID);

	}

	@Test(expected = InstanceNotCancellableException.class)
	public void TestDeleteNotRemovableDelivery()
			throws InputValidationException, InstanceNotFoundException,
			InstanceNotCancellableException {

		Customer customer1 = getValidCustomer("Ares");
		customer1 = deliveryService.addCustomer(customer1);
		Delivery delivery = getValidDelivery(customer1.getCustomerId());
		delivery = deliveryService.addDelivery(delivery);
		delivery.setStatus(DeliveryStatus.SENT);

		deliveryService.cancelDelivery(delivery.getDeliveryId());

	}

	@Test
	public void TestUpdateDelivery() throws InputValidationException,
			InstanceNotRemovableException, InstanceNotFoundException,
			StatusNotValidException {

		Customer customer1 = getValidCustomer("Ares");
		customer1 = deliveryService.addCustomer(customer1);
		Delivery delivery1 = getValidDelivery(customer1.getCustomerId());
		Delivery delivery2 = getValidDelivery(customer1.getCustomerId());
		delivery1 = deliveryService.addDelivery(delivery1);
		delivery2 = deliveryService.addDelivery(delivery2);

		assertEquals(DeliveryStatus.PENDING, delivery1.getStatus());
		deliveryService.updateDeliveryStatus(delivery1.getDeliveryId(),
				DeliveryStatus.SENT);
		assertEquals(DeliveryStatus.SENT, delivery1.getStatus());
		deliveryService.updateDeliveryStatus(delivery1.getDeliveryId(),
				DeliveryStatus.DELIVERED);
		assertEquals(DeliveryStatus.DELIVERED, delivery1.getStatus());
		deliveryService.updateDeliveryStatus(delivery2.getDeliveryId(),
				DeliveryStatus.SENT);
		deliveryService.updateDeliveryStatus(delivery2.getDeliveryId(),
				DeliveryStatus.REJECTED);
		assertEquals(DeliveryStatus.REJECTED, delivery2.getStatus());

	}

	@Test(expected = InstanceNotFoundException.class)
	public void TestUpdateNotExistentDelivery()
			throws InstanceNotFoundException, InputValidationException,
			StatusNotValidException {
		deliveryService.updateDeliveryStatus(NON_EXISTENT_DELIVERY_ID,
				DeliveryStatus.SENT);

	}

	@Test(expected = StatusNotValidException.class)
	public void TestUpdateInvalidStatusDeliveryPendingDelivered()
			throws InstanceNotRemovableException, InstanceNotFoundException,
			InputValidationException, StatusNotValidException {

		Customer customer1 = getValidCustomer("Ares");
		customer1 = deliveryService.addCustomer(customer1);
		Delivery delivery = getValidDelivery(customer1.getCustomerId());
		delivery = deliveryService.addDelivery(delivery);
		deliveryService.updateDeliveryStatus(delivery.getDeliveryId(),
				DeliveryStatus.DELIVERED);
	}

	@Test(expected = StatusNotValidException.class)
	public void TestUpdateInvalidStatusDeliveryPendingRejected()
			throws InstanceNotRemovableException, InstanceNotFoundException,
			InputValidationException, StatusNotValidException {

		Customer customer1 = getValidCustomer("Ares");
		customer1 = deliveryService.addCustomer(customer1);
		Delivery delivery = getValidDelivery(customer1.getCustomerId());
		delivery = deliveryService.addDelivery(delivery);
		deliveryService.updateDeliveryStatus(delivery.getDeliveryId(),
				DeliveryStatus.REJECTED);

	}

	@Test(expected = StatusNotValidException.class)
	public void TestUpdateInvalidStatusDeliveryPendingCancelled()
			throws InstanceNotRemovableException, InstanceNotFoundException,
			InputValidationException, StatusNotValidException {

		Customer customer1 = getValidCustomer("Ares");
		customer1 = deliveryService.addCustomer(customer1);
		Delivery delivery = getValidDelivery(customer1.getCustomerId());
		delivery = deliveryService.addDelivery(delivery);
		deliveryService.updateDeliveryStatus(delivery.getDeliveryId(),
				DeliveryStatus.CANCELLED);
	}

	@Test(expected = StatusNotValidException.class)
	public void TestUpdateInvalidStatusDeliverySentPending()
			throws InstanceNotRemovableException, InstanceNotFoundException,
			InputValidationException, StatusNotValidException {

		Customer customer1 = getValidCustomer("Ares");
		customer1 = deliveryService.addCustomer(customer1);
		Delivery delivery = getValidDelivery(customer1.getCustomerId());
		delivery = deliveryService.addDelivery(delivery);
		deliveryService.updateDeliveryStatus(delivery.getDeliveryId(),
				DeliveryStatus.SENT);
		deliveryService.updateDeliveryStatus(delivery.getDeliveryId(),
				DeliveryStatus.PENDING);
	}

	@Test(expected = StatusNotValidException.class)
	public void TestUpdateInvalidStatusDeliverySentCancelled()
			throws InstanceNotRemovableException, InstanceNotFoundException,
			InputValidationException, StatusNotValidException {

		Customer customer1 = getValidCustomer("Ares");
		customer1 = deliveryService.addCustomer(customer1);
		Delivery delivery = getValidDelivery(customer1.getCustomerId());
		delivery = deliveryService.addDelivery(delivery);
		deliveryService.updateDeliveryStatus(delivery.getDeliveryId(),
				DeliveryStatus.SENT);
		deliveryService.updateDeliveryStatus(delivery.getDeliveryId(),
				DeliveryStatus.CANCELLED);
	}

	@Test(expected = StatusNotValidException.class)
	public void TestUpdateInvalidStatusDeliveryDeliveredPending()
			throws InstanceNotRemovableException, InstanceNotFoundException,
			InputValidationException, StatusNotValidException {

		Customer customer1 = getValidCustomer("Ares");
		customer1 = deliveryService.addCustomer(customer1);
		Delivery delivery = getValidDelivery(customer1.getCustomerId());
		delivery = deliveryService.addDelivery(delivery);
		deliveryService.updateDeliveryStatus(delivery.getDeliveryId(),
				DeliveryStatus.SENT);
		deliveryService.updateDeliveryStatus(delivery.getDeliveryId(),
				DeliveryStatus.DELIVERED);
		deliveryService.updateDeliveryStatus(delivery.getDeliveryId(),
				DeliveryStatus.PENDING);
	}

	@Test(expected = StatusNotValidException.class)
	public void TestUpdateInvalidStatusDeliveryDeliveredSent()
			throws InstanceNotRemovableException, InstanceNotFoundException,
			InputValidationException, StatusNotValidException {

		Customer customer1 = getValidCustomer("Ares");
		customer1 = deliveryService.addCustomer(customer1);
		Delivery delivery = getValidDelivery(customer1.getCustomerId());
		delivery = deliveryService.addDelivery(delivery);
		deliveryService.updateDeliveryStatus(delivery.getDeliveryId(),
				DeliveryStatus.SENT);
		deliveryService.updateDeliveryStatus(delivery.getDeliveryId(),
				DeliveryStatus.DELIVERED);
		deliveryService.updateDeliveryStatus(delivery.getDeliveryId(),
				DeliveryStatus.SENT);
	}

	@Test(expected = StatusNotValidException.class)
	public void TestUpdateInvalidStatusDeliveryDeliveredRejected()
			throws InstanceNotRemovableException, InstanceNotFoundException,
			InputValidationException, StatusNotValidException {

		Customer customer1 = getValidCustomer("Ares");
		customer1 = deliveryService.addCustomer(customer1);
		Delivery delivery = getValidDelivery(customer1.getCustomerId());
		delivery = deliveryService.addDelivery(delivery);
		deliveryService.updateDeliveryStatus(delivery.getDeliveryId(),
				DeliveryStatus.SENT);
		deliveryService.updateDeliveryStatus(delivery.getDeliveryId(),
				DeliveryStatus.DELIVERED);
		deliveryService.updateDeliveryStatus(delivery.getDeliveryId(),
				DeliveryStatus.REJECTED);
	}

	@Test(expected = StatusNotValidException.class)
	public void TestUpdateInvalidStatusDeliveryDeliveredCancelled()
			throws InstanceNotRemovableException, InstanceNotFoundException,
			InputValidationException, StatusNotValidException {

		Customer customer1 = getValidCustomer("Ares");
		customer1 = deliveryService.addCustomer(customer1);
		Delivery delivery = getValidDelivery(customer1.getCustomerId());
		delivery = deliveryService.addDelivery(delivery);
		deliveryService.updateDeliveryStatus(delivery.getDeliveryId(),
				DeliveryStatus.SENT);
		deliveryService.updateDeliveryStatus(delivery.getDeliveryId(),
				DeliveryStatus.DELIVERED);
		deliveryService.updateDeliveryStatus(delivery.getDeliveryId(),
				DeliveryStatus.CANCELLED);
	}

	@Test(expected = StatusNotValidException.class)
	public void TestUpdateInvalidStatusDeliveryRejectedPending()
			throws InstanceNotRemovableException, InstanceNotFoundException,
			InputValidationException, StatusNotValidException {

		Customer customer1 = getValidCustomer("Ares");
		customer1 = deliveryService.addCustomer(customer1);
		Delivery delivery = getValidDelivery(customer1.getCustomerId());
		delivery = deliveryService.addDelivery(delivery);
		deliveryService.updateDeliveryStatus(delivery.getDeliveryId(),
				DeliveryStatus.SENT);
		deliveryService.updateDeliveryStatus(delivery.getDeliveryId(),
				DeliveryStatus.REJECTED);
		deliveryService.updateDeliveryStatus(delivery.getDeliveryId(),
				DeliveryStatus.PENDING);
	}

	@Test(expected = StatusNotValidException.class)
	public void TestUpdateInvalidStatusDeliveryRejectedSent()
			throws InstanceNotRemovableException, InstanceNotFoundException,
			InputValidationException, StatusNotValidException {

		Customer customer1 = getValidCustomer("Ares");
		customer1 = deliveryService.addCustomer(customer1);
		Delivery delivery = getValidDelivery(customer1.getCustomerId());
		delivery = deliveryService.addDelivery(delivery);
		deliveryService.updateDeliveryStatus(delivery.getDeliveryId(),
				DeliveryStatus.SENT);
		deliveryService.updateDeliveryStatus(delivery.getDeliveryId(),
				DeliveryStatus.REJECTED);
		deliveryService.updateDeliveryStatus(delivery.getDeliveryId(),
				DeliveryStatus.SENT);
	}

	@Test(expected = StatusNotValidException.class)
	public void TestUpdateInvalidStatusDeliveryRejectedDelivered()
			throws InstanceNotRemovableException, InstanceNotFoundException,
			InputValidationException, StatusNotValidException {

		Customer customer1 = getValidCustomer("Ares");
		customer1 = deliveryService.addCustomer(customer1);
		Delivery delivery = getValidDelivery(customer1.getCustomerId());
		delivery = deliveryService.addDelivery(delivery);
		deliveryService.updateDeliveryStatus(delivery.getDeliveryId(),
				DeliveryStatus.SENT);
		deliveryService.updateDeliveryStatus(delivery.getDeliveryId(),
				DeliveryStatus.REJECTED);
		deliveryService.updateDeliveryStatus(delivery.getDeliveryId(),
				DeliveryStatus.DELIVERED);
	}

	@Test(expected = StatusNotValidException.class)
	public void TestUpdateInvalidStatusDeliveryRejectedCancelled()
			throws InstanceNotRemovableException, InstanceNotFoundException,
			InputValidationException, StatusNotValidException {

		Customer customer1 = getValidCustomer("Ares");
		customer1 = deliveryService.addCustomer(customer1);
		Delivery delivery = getValidDelivery(customer1.getCustomerId());
		delivery = deliveryService.addDelivery(delivery);
		deliveryService.updateDeliveryStatus(delivery.getDeliveryId(),
				DeliveryStatus.SENT);
		deliveryService.updateDeliveryStatus(delivery.getDeliveryId(),
				DeliveryStatus.REJECTED);
		deliveryService.updateDeliveryStatus(delivery.getDeliveryId(),
				DeliveryStatus.CANCELLED);
	}

	@Test(expected = StatusNotValidException.class)
	public void TestUpdateInvalidStatusDeliveryRemovedPending()
			throws InstanceNotFoundException, InputValidationException,
			InstanceNotCancellableException, StatusNotValidException {

		Customer customer1 = getValidCustomer("Ares");
		customer1 = deliveryService.addCustomer(customer1);
		Delivery delivery = getValidDelivery(customer1.getCustomerId());
		delivery = deliveryService.addDelivery(delivery);
		deliveryService.cancelDelivery(delivery.getDeliveryId());
		deliveryService.updateDeliveryStatus(delivery.getDeliveryId(),
				DeliveryStatus.PENDING);

	}

	@Test(expected = StatusNotValidException.class)
	public void TestUpdateInvalidStatusDeliveryRemovedSent()
			throws InstanceNotFoundException, InputValidationException,
			InstanceNotCancellableException, StatusNotValidException {

		Customer customer1 = getValidCustomer("Ares");
		customer1 = deliveryService.addCustomer(customer1);
		Delivery delivery = getValidDelivery(customer1.getCustomerId());
		delivery = deliveryService.addDelivery(delivery);
		deliveryService.cancelDelivery(delivery.getDeliveryId());
		deliveryService.updateDeliveryStatus(delivery.getDeliveryId(),
				DeliveryStatus.SENT);

	}

	@Test(expected = StatusNotValidException.class)
	public void TestUpdateInvalidStatusDeliveryRemovedDelivered()
			throws InstanceNotFoundException, InputValidationException,
			InstanceNotCancellableException, StatusNotValidException {

		Customer customer1 = getValidCustomer("Ares");
		customer1 = deliveryService.addCustomer(customer1);
		Delivery delivery = getValidDelivery(customer1.getCustomerId());
		delivery = deliveryService.addDelivery(delivery);
		deliveryService.cancelDelivery(delivery.getDeliveryId());
		deliveryService.updateDeliveryStatus(delivery.getDeliveryId(),
				DeliveryStatus.DELIVERED);

	}

	@Test(expected = StatusNotValidException.class)
	public void TestUpdateInvalidStatusDeliveryRemovedRejected()
			throws InstanceNotFoundException, InputValidationException,
			InstanceNotCancellableException, StatusNotValidException {

		Customer customer1 = getValidCustomer("Ares");
		customer1 = deliveryService.addCustomer(customer1);
		Delivery delivery = getValidDelivery(customer1.getCustomerId());
		delivery = deliveryService.addDelivery(delivery);
		deliveryService.cancelDelivery(delivery.getDeliveryId());
		deliveryService.updateDeliveryStatus(delivery.getDeliveryId(),
				DeliveryStatus.REJECTED);

	}

	@Test(expected = StatusNotValidException.class)
	public void TestUpdateInvalidStatusDeliveryRemovedCancelled()
			throws InstanceNotFoundException,
			InputValidationException, InstanceNotCancellableException, StatusNotValidException {

		Customer customer1 = getValidCustomer("Ares");
		customer1 = deliveryService.addCustomer(customer1);
		Delivery delivery = getValidDelivery(customer1.getCustomerId());
		delivery = deliveryService.addDelivery(delivery);
		deliveryService.cancelDelivery(delivery.getDeliveryId());
		deliveryService.updateDeliveryStatus(delivery.getDeliveryId(),
				DeliveryStatus.CANCELLED);

	}

}

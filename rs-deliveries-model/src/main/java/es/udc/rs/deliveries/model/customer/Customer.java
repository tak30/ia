package es.udc.rs.deliveries.model.customer;

import java.util.Calendar;

public class Customer {

    private Long customerId;
    private String name;
    private String cif;
    private String direction;
    private Integer postcode;
    private Calendar creationDate;
    
	public Customer(String name, String cif,
			String direction, Integer postcode) {
		super();
		this.name = name;
		this.cif = cif;
		this.direction = direction;
		this.postcode = postcode;
		this.creationDate = Calendar.getInstance();
		this.creationDate.set(Calendar.MILLISECOND, 0);

	}

	public Customer(Long customerId, String name,
			String cif, String direction, Integer postcode, Calendar creationDate) {
		this(name, cif, direction, postcode);
		this.customerId = customerId;
		this.creationDate = creationDate;
	}
	
	public Boolean contains(String keywords){
		String[] l = keywords.split(" ");
		for (String element : l) {
			if (this.name.toLowerCase().contains(element.toLowerCase())) {
				return true;
			}
		}
		return false;
	}
	
	//Getters, setters, equals and hashcode

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public String getCif() {
		return cif;
	}

	public void setCif(String cif) {
		this.cif = cif;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public Integer getPostcode() {
		return postcode;
	}

	public void setPostcode(Integer postcode) {
		this.postcode = postcode;
	}

	public Calendar getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
		this.creationDate.set(Calendar.MILLISECOND, 0);

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cif == null) ? 0 : cif.hashCode());
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result
				+ ((customerId == null) ? 0 : customerId.hashCode());
		result = prime * result
				+ ((direction == null) ? 0 : direction.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((postcode == null) ? 0 : postcode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		if (cif == null) {
			if (other.cif != null)
				return false;
		} else if (!cif.equals(other.cif))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (customerId == null) {
			if (other.customerId != null)
				return false;
		} else if (!customerId.equals(other.customerId))
			return false;
		if (direction == null) {
			if (other.direction != null)
				return false;
		} else if (!direction.equals(other.direction))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (postcode == null) {
			if (other.postcode != null)
				return false;
		} else if (!postcode.equals(other.postcode))
			return false;
		return true;
	}

	

	
	
}
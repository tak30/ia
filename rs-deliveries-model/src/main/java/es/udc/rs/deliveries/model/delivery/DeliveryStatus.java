package es.udc.rs.deliveries.model.delivery;

public enum DeliveryStatus {
	PENDING, SENT, DELIVERED, REJECTED, CANCELLED
}

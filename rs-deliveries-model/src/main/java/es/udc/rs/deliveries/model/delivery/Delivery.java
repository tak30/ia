package es.udc.rs.deliveries.model.delivery;

import java.util.Calendar;

public class Delivery {

	private Long customerId;
	private Long deliveryId;
	private Long deliveryClientId;
	private int estimatedTime;
	private String direction;
	private Integer postcode;
	private Calendar creationDate;
	private Calendar deliveredDate;
	private DeliveryStatus status;

	public Delivery(Long customerId, Long deliveryClientId, String direction,
			Integer postcode) {
		super();
		this.customerId = customerId;
		this.deliveryClientId = deliveryClientId;
		this.estimatedTime = 48;
		this.direction = direction;
		this.postcode = postcode;
		this.creationDate = Calendar.getInstance();
		this.creationDate.set(Calendar.MILLISECOND, 0);
		this.status = DeliveryStatus.PENDING;
	}

	public Delivery(Long customerId, Long deliveryId, Long deliveryClientId,
			String direction, Integer postcode) {
		this(customerId, deliveryClientId, direction, postcode);
		this.deliveryId = deliveryId;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Long getDeliveryId() {
		return deliveryId;
	}

	public void setDeliveryId(Long deliveryId) {
		this.deliveryId = deliveryId;
	}

	public Long getDeliveryClientId() {
		return deliveryClientId;
	}

	public void setDeliveryClientId(Long deliveryClientId) {
		this.deliveryClientId = deliveryClientId;
	}

	public int getEstimatedTime() {
		return estimatedTime;
	}

	public void setEstimatedTime(int estimatedTime) {
		this.estimatedTime = estimatedTime;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public Integer getPostcode() {
		return postcode;
	}

	public void setPostcode(Integer postcode) {
		this.postcode = postcode;
	}

	public Calendar getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
		if (this.creationDate != null) {
			this.creationDate.set(Calendar.MILLISECOND, 0);
		}
	}

	public DeliveryStatus getStatus() {
		return status;
	}

	public void setStatus(DeliveryStatus status) {
		this.status = status;
	}

	public Calendar getDeliveredDate() {
		return deliveredDate;
	}

	public void setDeliveredDate(Calendar deliveredDate) {
		this.deliveredDate = deliveredDate;
		if (this.deliveredDate != null) {
			this.deliveredDate.set(Calendar.MILLISECOND, 0);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result
				+ ((customerId == null) ? 0 : customerId.hashCode());
		result = prime * result
				+ ((deliveredDate == null) ? 0 : deliveredDate.hashCode());
		result = prime
				* result
				+ ((deliveryClientId == null) ? 0 : deliveryClientId.hashCode());
		result = prime * result
				+ ((deliveryId == null) ? 0 : deliveryId.hashCode());
		result = prime * result
				+ ((direction == null) ? 0 : direction.hashCode());
		result = prime * result + estimatedTime;
		result = prime * result
				+ ((postcode == null) ? 0 : postcode.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Delivery other = (Delivery) obj;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (customerId == null) {
			if (other.customerId != null)
				return false;
		} else if (!customerId.equals(other.customerId))
			return false;
		if (deliveredDate == null) {
			if (other.deliveredDate != null)
				return false;
		} else if (!deliveredDate.equals(other.deliveredDate))
			return false;
		if (deliveryClientId == null) {
			if (other.deliveryClientId != null)
				return false;
		} else if (!deliveryClientId.equals(other.deliveryClientId))
			return false;
		if (deliveryId == null) {
			if (other.deliveryId != null)
				return false;
		} else if (!deliveryId.equals(other.deliveryId))
			return false;
		if (direction == null) {
			if (other.direction != null)
				return false;
		} else if (!direction.equals(other.direction))
			return false;
		if (estimatedTime != other.estimatedTime)
			return false;
		if (postcode == null) {
			if (other.postcode != null)
				return false;
		} else if (!postcode.equals(other.postcode))
			return false;
		if (status != other.status)
			return false;
		return true;
	}

}
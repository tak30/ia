package es.udc.rs.deliveries.model.deliveryservice;

import java.util.List;

import es.udc.rs.deliveries.model.customer.Customer;
import es.udc.rs.deliveries.model.delivery.Delivery;
import es.udc.rs.deliveries.model.delivery.DeliveryStatus;
import es.udc.ws.util.exceptions.InputValidationException;
import es.udc.ws.util.exceptions.InstanceNotFoundException;
import es.udc.rs.deliveries.util.exceptions.InstanceNotCancellableException;
import es.udc.rs.deliveries.util.exceptions.InstanceNotRemovableException;
import es.udc.rs.deliveries.util.exceptions.StatusNotValidException;

public interface DeliveryService {

	public Customer addCustomer(Customer client)
			throws InputValidationException;

	public Customer findCustomer(Long clientId)
			throws InstanceNotFoundException;

	public List<Customer> findCustomerByKeywords(String keywords, int index,
			int size);

	public void deleteCustomer(Long clientId)
			throws InstanceNotRemovableException, InstanceNotFoundException;

	public void updateCustomer(Customer customer)
			throws InstanceNotFoundException, InputValidationException;

	public Delivery addDelivery(Delivery delivery)
			throws InputValidationException, InstanceNotFoundException;

	public void updateDeliveryStatus(Long deliveryId, DeliveryStatus status)
			throws InstanceNotFoundException, StatusNotValidException;

	public void cancelDelivery(Long deliveryId)
			throws InstanceNotCancellableException, InstanceNotFoundException;

	public Delivery findDelivery(Long deliveryId)
			throws InstanceNotFoundException;

	public List<Delivery> findDeliveries(Long customerId, 
			DeliveryStatus status, int index, int size)
		   throws InstanceNotFoundException;

}

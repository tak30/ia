package es.udc.rs.deliveries.model.deliveryservice;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import java.util.Set;

import es.udc.rs.deliveries.model.customer.Customer;
import es.udc.rs.deliveries.model.delivery.Delivery;
import es.udc.rs.deliveries.model.delivery.DeliveryStatus;
import es.udc.rs.deliveries.util.exceptions.InstanceNotCancellableException;
import es.udc.rs.deliveries.util.exceptions.InstanceNotRemovableException;
import es.udc.rs.deliveries.util.exceptions.StatusNotValidException;
import es.udc.ws.util.exceptions.InputValidationException;
import es.udc.ws.util.exceptions.InstanceNotFoundException;
import es.udc.ws.util.validation.PropertyValidator;

public class MockDeliveryService implements DeliveryService {

	private Map<Long, Customer> customersMap = new LinkedHashMap<Long, Customer>();

	private long lastCustomerId = 0;

	private synchronized long getNextCustomerId() {
		return ++lastCustomerId;
	}

	private Map<Long, Delivery> deliveriesMap = new LinkedHashMap<Long, Delivery>();

	private long lastDeliveryId = 0;

	private synchronized long getNextDeliveryId() {
		return ++lastDeliveryId;
	}

	// Customer related

	private static void validateCustomer(Customer customer)
			throws InputValidationException {
		PropertyValidator.validateMandatoryString("name", customer.getName());
		PropertyValidator.validateMandatoryString("cif", customer.getCif());
		PropertyValidator.validateMandatoryString("direction",
				customer.getDirection());
		// TODO Validate Postcode (not integer method in PropertyValidator
	}

	@Override
	public Customer addCustomer(Customer customer)
			throws InputValidationException {
		validateCustomer(customer);
		customer.setCustomerId(getNextCustomerId());
		customersMap.put(customer.getCustomerId(), customer);
		return customer;
	}

	@Override
	public Customer findCustomer(Long customerId)
			throws InstanceNotFoundException {
		Customer customer = customersMap.get(customerId);
		if (customer == null) {
			throw new InstanceNotFoundException(customerId,
					Customer.class.getName());
		}
		return customer;
	}

	@Override
	public List<Customer> findCustomerByKeywords(String keywords, int index,
			int size) {
		index--;
		List<Customer> customerList = new ArrayList<Customer>();
		Iterator<Customer> iter = customersMap.values().iterator();
		int j = 0;
		while (iter.hasNext()) {
			if (j < index) {
				iter.next();
				j++;
				continue;
			}
			j++;
			Customer currentCustomer = iter.next();
			if (currentCustomer.contains(keywords)) {
				customerList.add(currentCustomer);
			}
			if (size >= 0 && customerList.size() >= size) {
				break;
			}
		}

		return customerList;

	}

	@Override
	public synchronized void deleteCustomer(Long customerId)
			throws InstanceNotRemovableException, InstanceNotFoundException {
		Customer customer = customersMap.get(customerId);
		if (customer == null) {
			throw new InstanceNotFoundException(customerId,
					Customer.class.getName());
		}
		if (findDeliveries(customerId, null, -1, -1).size() > 0) {
			throw new InstanceNotRemovableException(customerId,
					Customer.class.getName());
		}
		customersMap.remove(customerId);
	}

	@Override
	public synchronized void updateCustomer(Customer customer)
			throws InstanceNotFoundException, InputValidationException {
		PropertyValidator.validateMandatoryString("name", customer.getName());
		PropertyValidator.validateMandatoryString("cif", customer.getCif());
		PropertyValidator.validateMandatoryString("direction",
				customer.getDirection());
		Customer oldCustomer = customersMap.get(customer.getCustomerId());
		if (oldCustomer == null) {
			throw new InstanceNotFoundException(customer.getCustomerId(),
					Customer.class.getName());
		}
		oldCustomer.setName(customer.getName());
		oldCustomer.setCif(customer.getCif());
		oldCustomer.setDirection(customer.getDirection());
		oldCustomer.setPostcode(customer.getPostcode());
		customersMap.put(customer.getCustomerId(), oldCustomer);
	}

	// Delivery related

	private static void validateDelivery(Delivery delivery)
			throws InputValidationException {
		PropertyValidator.validateMandatoryString("direction",
				delivery.getDirection());
	}

	@Override
	public Delivery addDelivery(Delivery delivery)
			throws InputValidationException, InstanceNotFoundException {

		validateDelivery(delivery);
		findCustomer(delivery.getCustomerId());
		delivery.setDeliveryId(getNextDeliveryId());
		deliveriesMap.put(delivery.getDeliveryId(), delivery);
		return delivery;
	}

	@Override
	public synchronized void updateDeliveryStatus(Long deliveryId,
			DeliveryStatus status) throws InstanceNotFoundException,
			StatusNotValidException {
		Delivery delivery = findDelivery(deliveryId);
		if ((status.equals(DeliveryStatus.SENT))
				&& (delivery.getStatus().equals(DeliveryStatus.PENDING))) {
			delivery.setStatus(status);
		} else if ((status.equals(DeliveryStatus.DELIVERED))
				&& (delivery.getStatus().equals(DeliveryStatus.SENT))) {
			delivery.setStatus(status);
			delivery.setDeliveredDate(Calendar.getInstance());
		} else if ((status.equals(DeliveryStatus.REJECTED))
				&& (delivery.getStatus().equals(DeliveryStatus.SENT))) {
			delivery.setStatus(status);
		} else
			throw new StatusNotValidException("Invalid status");
	}

	@Override
	public synchronized void cancelDelivery(Long deliveryId)
			throws InstanceNotFoundException, InstanceNotCancellableException {
		Delivery delivery = deliveriesMap.get(deliveryId);
		if (delivery == null) {
			throw new InstanceNotFoundException(deliveryId,
					Delivery.class.getName());
		}
		if (delivery.getStatus() == DeliveryStatus.PENDING) {
			delivery.setStatus(DeliveryStatus.CANCELLED);
		} else {
			throw new InstanceNotCancellableException(deliveryId,
					Delivery.class.getName());
		}
	}

	@Override
	public Delivery findDelivery(Long deliveryId)
			throws InstanceNotFoundException {
		Delivery delivery = deliveriesMap.get(deliveryId);
		if (delivery == null) {
			throw new InstanceNotFoundException(deliveryId,
					Delivery.class.getName());
		}
		return delivery;
	}

	@Override
	public List<Delivery> findDeliveries(Long customerId,
			DeliveryStatus status, int index, int size)
			throws InstanceNotFoundException {
		index--;
		List<Delivery> deliveryList = new ArrayList<Delivery>();
		if (customerId != null) {
			findCustomer(customerId);
		}
		Iterator<Delivery> iter = deliveriesMap.values().iterator();
		int j = 0;
		while (iter.hasNext()) {
			if (j < index) {
				iter.next();
				j++;
				continue;
			}
			j++;
			Delivery currentDelivery = iter.next();
			if (customerId != null && status == null) {
				if (currentDelivery.getCustomerId() == customerId) {
					deliveryList.add(currentDelivery);
				}
			} else if (customerId == null && status != null) {
				if (currentDelivery.getStatus().equals(status)) {
					deliveryList.add(currentDelivery);
				}
			} else if (customerId == null && status == null) {
				deliveryList.add(currentDelivery);

			} else if ((currentDelivery.getCustomerId() == customerId)
					&& (status.equals(currentDelivery.getStatus()))) {
				deliveryList.add(currentDelivery);
			}
			if (size >= 0 && deliveryList.size() >= size) {
				break;
			}
		}
		return deliveryList;
	}

}

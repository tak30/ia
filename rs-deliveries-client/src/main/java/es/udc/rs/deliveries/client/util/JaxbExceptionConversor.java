package es.udc.rs.deliveries.client.util;


import es.udc.rs.deliveries.client.service.rest.dto.InputValidationExceptionDtoJaxb;
import es.udc.rs.deliveries.client.service.rest.dto.InstanceNotCancellableExceptionDtoJaxb;
import es.udc.rs.deliveries.client.service.rest.dto.InstanceNotFoundExceptionDtoJaxb;
import es.udc.rs.deliveries.client.service.rest.dto.InstanceNotRemovableExceptionDtoJaxb;
import es.udc.rs.deliveries.client.service.rest.dto.StatusNotValidExceptionDtoJaxb;
import es.udc.rs.deliveries.util.exceptions.InstanceNotCancellableException;
import es.udc.rs.deliveries.util.exceptions.InstanceNotRemovableException;
import es.udc.rs.deliveries.util.exceptions.StatusNotValidException;
import es.udc.ws.util.exceptions.InputValidationException;
import es.udc.ws.util.exceptions.InstanceNotFoundException;

public class JaxbExceptionConversor {

	public static InstanceNotFoundException toInstanceNotFoundException(
			InstanceNotFoundExceptionDtoJaxb exDto) {
		return new InstanceNotFoundException(exDto.getInstanceId(),
				exDto.getInstanceType());
	}
	
	public static InputValidationException toInputValidationException(
			InputValidationExceptionDtoJaxb exDto) {
		return new InputValidationException(exDto.getMessage());
	}
	
	public static StatusNotValidException toStatusNotValidException(
			StatusNotValidExceptionDtoJaxb exDto) {
		return new StatusNotValidException(exDto.getMessage());
	}
	
	public static InstanceNotCancellableException toInstanceNotCancellableException(
			InstanceNotCancellableExceptionDtoJaxb exDto) {
		return new InstanceNotCancellableException(exDto.getInstanceId(),
				exDto.getInstanceType());
	}
	
	public static InstanceNotRemovableException toInstanceNotRemovableException(
			InstanceNotRemovableExceptionDtoJaxb exDto) {
		return new InstanceNotRemovableException(exDto.getInstanceId(),
				exDto.getInstanceType());
	}

}

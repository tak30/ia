package es.udc.rs.deliveries.client.ui;

import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.List;

import es.udc.rs.deliveries.client.dto.CustomerDetailsDto;
import es.udc.rs.deliveries.client.dto.CustomerDto;
import es.udc.rs.deliveries.client.dto.DeliveryDetailsDto;
import es.udc.rs.deliveries.client.dto.DeliveryDto;
import es.udc.rs.deliveries.client.dto.DeliveryStatusDto;
import es.udc.rs.deliveries.client.rest.ClientDeliveryServiceFactory;
import es.udc.rs.deliveries.client.service.ClientDeliveryService;
import es.udc.ws.util.exceptions.InputValidationException;
import es.udc.ws.util.exceptions.InstanceNotFoundException;

public class DeliveryServiceClient {

	public static void main(String[] args) {

		if (args.length == 0) {
			printUsageAndExit();
		}
		ClientDeliveryService clientDeliveryService = ClientDeliveryServiceFactory
				.getService();
		if ("-ca".equalsIgnoreCase(args[0])) {
			validateArgs(args, 5, new int[] {});

			try {
				Long clientId = clientDeliveryService
						.addCustomer(new CustomerDetailsDto(null, args[1], args[2],
								args[3], Integer.parseInt(args[4])));

				System.out.println("Customer " + clientId + " "
						+ "created sucessfully");

			} catch (NumberFormatException | InputValidationException ex) {
				ex.printStackTrace(System.err);
			} catch (Exception ex) {
				ex.printStackTrace(System.err);
			}

		} else if ("-cf".equalsIgnoreCase(args[0])) {
			validateArgs(args, 2, new int[] { 1 });

			try {
				CustomerDetailsDto customerDto = clientDeliveryService
						.findCustomer(Long.parseLong(args[1]));
				System.out.println("Id: " + customerDto.getCustomerId()
						+ " Name: " + customerDto.getName() + " Cif: "
						+ customerDto.getCif() + " Direction: "
						+ customerDto.getDirection() + " Postcode: "
						+ customerDto.getPostcode());
			} catch (NumberFormatException ex) {
				ex.printStackTrace(System.err);
			} catch (Exception ex) {
				ex.printStackTrace(System.err);
			}

		} else if ("-cu".equalsIgnoreCase(args[0])) {
			validateArgs(args, 6, new int[] { });

			try {
				clientDeliveryService.updateCustomer(new CustomerDetailsDto(Long
						.valueOf(args[1]), args[2], args[3], args[4], Integer
						.valueOf(args[5])));

				System.out.println("Customer " + args[1]
						+ " updated sucessfully");

			} catch (NumberFormatException | InputValidationException
					| InstanceNotFoundException ex) {
				ex.printStackTrace(System.err);
			} catch (Exception ex) {
				ex.printStackTrace(System.err);
			}

		} else if ("-cr".equalsIgnoreCase(args[0])) {
			validateArgs(args, 2, new int[] { 1 });

			try {
				clientDeliveryService.deleteCustomer((Long.parseLong(args[1])));

				System.out.println("Customer with id " + args[1]
						+ " removed sucessfully");

			} catch (NumberFormatException | InstanceNotFoundException ex) {
				ex.printStackTrace(System.err);
			} catch (Exception ex) {
				ex.printStackTrace(System.err);
			}

		} else if ("-ck".equalsIgnoreCase(args[0])) {
			String index = "-1";
			String count = "-1";
			if (args.length == 2) {
				validateArgs(args, 2, new int[] {});
			} else if (args.length == 4) {
				validateArgs(args, 4, new int[] {});
				if (Integer.valueOf(args[2]) >= 1
						&& Integer.valueOf(args[3]) >= 1) {
					index = args[2];
					count = args[3];
				}
			}

			try {
				List<CustomerDto> customer = clientDeliveryService
						.findCustomerByKeywords(args[1], index, count);
				System.out.println("Found " + customer.size()
						+ " customer(s) with keywords '" + args[1] + "'");
				for (int i = 0; i < customer.size(); i++) {
					CustomerDto customerDto = customer.get(i);
					System.out.println("Id: " + customerDto.getCustomerId()
							+ " Name: " + customerDto.getName());
				}
			} catch (Exception ex) {
				ex.printStackTrace(System.err);
			}

		} else if ("-da".equalsIgnoreCase(args[0])) {

			validateArgs(args, 5, new int[] {});
			try {
				Long deliveryId = clientDeliveryService
						.addDelivery(new DeliveryDetailsDto(Long.valueOf(args[1]),
								null, Long.valueOf(args[2]), null, null, args[3],
								Integer.parseInt(args[4]), null, null));

				System.out.println("Delivery " + deliveryId + " "
						+ "created sucessfully");

			} catch (NumberFormatException ex) {
				ex.printStackTrace(System.err);
			} catch (Exception ex) {
				ex.printStackTrace(System.err);
			}

		} else if ("-df".equalsIgnoreCase(args[0])) {
			validateArgs(args, 2, new int[] { 1 });
			try {
				DeliveryDetailsDto deliveryDto = clientDeliveryService
						.findDelivery(Long.parseLong(args[1]));
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
				dateFormat.setTimeZone(deliveryDto.getEstimatedDate()
						.getTimeZone());

				String out = "Id: "
						+ deliveryDto.getDeliveryId()
						+ " Customer Id: "
						+ deliveryDto.getCustomerId()
						+ " Delivery Client Id: "
						+ deliveryDto.getDeliveryClientId()
						+ " Creation date: "
						+ dateFormat.format(deliveryDto.getCreationDate()
								.getTime())
						+ " Estimated date: "
						+ dateFormat.format(deliveryDto.getEstimatedDate()
								.getTime()) + " Direction : "
						+ deliveryDto.getDirection() + " Postcode : "
						+ deliveryDto.getPostcode() + " Status : "
						+ deliveryDto.getStatus().toString();
				if (deliveryDto.getDeliveredDate() != null) {
					out += " Delivered Date : "
							+ dateFormat.format(deliveryDto.getDeliveredDate()
									.getTime());
				}
				System.out.println(out);
			} catch (NumberFormatException ex) {
				ex.printStackTrace(System.err);
			} catch (Exception ex) {
				ex.printStackTrace(System.err);
			}

		} else if ("-ds".equalsIgnoreCase(args[0])) {
			DeliveryStatusDto status = null;
			if (!(args[2].equals("null"))) {
				status = DeliveryStatusDto.valueOf(args[2]);
			}
			try {
				List<DeliveryDto> deliveries = clientDeliveryService
						.findDeliveries(Long.valueOf(args[1]), status,
								Integer.valueOf(args[3]),
								Integer.valueOf(args[4]));
				System.out.println("Found " + deliveries.size()
						+ " delivery(s) with ID and Status " + args[1] + " and "
						+ args[2] + "'");
				
				for (int i = 0; i < deliveries.size(); i++) {
					DeliveryDto deliveryDto = deliveries.get(i);
					System.out.println("Delivery Id: "
							+ deliveryDto.getDeliveryId() + " Customer Id:  "
							+ deliveryDto.getCustomerId()
							+ " Status : "
							+ deliveryDto.getStatus().toString());
				}
			} catch (Exception ex) {
				ex.printStackTrace(System.err);
			}

		} else if ("-du".equalsIgnoreCase(args[0])) {
			validateArgs(args, 3, new int[] {});

			try {
				clientDeliveryService.updateDeliveryStatus(
						Long.valueOf(args[1]),
						DeliveryStatusDto.valueOf(args[2]));

				System.out.println("Delivery " + args[1] + " "
						+ "updated sucessfully");

			} catch (NumberFormatException ex) {
				ex.printStackTrace(System.err);
			} catch (Exception ex) {
				ex.printStackTrace(System.err);
			}

		} else if ("-dc".equalsIgnoreCase(args[0])) {
			validateArgs(args, 2, new int[] { 1 });

			try {
				clientDeliveryService.cancelDelivery((Long.parseLong(args[1])));

				System.out.println("Delivery with id " + args[1]
						+ " cancelled sucessfully");

			} catch (NumberFormatException ex) {
				ex.printStackTrace(System.err);
			} catch (Exception ex) {
				ex.printStackTrace(System.err);
			}

		}

	}

	public static void validateArgs(String[] args, int expectedArgs,
			int[] numericArguments) {
		if (expectedArgs != args.length) {
			printUsageAndExit();
		}
		for (int i = 0; i < numericArguments.length; i++) {
			int position = numericArguments[i];
			try {
				Double.parseDouble(args[position]);
			} catch (NumberFormatException n) {
				printUsageAndExit();
			}
		}
	}


	public static void printUsageAndExit() {
		printUsage();
		System.exit(-1);
	}


	public static void printUsage() {
		System.err
				.println("Usage:\n"
						+ "    [addCustomer]    DeliveryServiceClient -ca <name> <cif> <direction> <postcode>\n"
						+ "    [findCustomerById]   DeliveryServiceClient -cf <customerId>\n"
						+ "    [findCustomerByKeywords]    DeliveryServiceClient -ck <keyword> <index> <count>\n"
						+ "    [updateCustomer]    DeliveryServiceClient -cu <customerId> <name> <cif> <direction> <postcode>\n"
						+ "    [removeCustomer]    DeliveryServiceClient -cr <customerId>\n"
						+ "    [addDelivery]    DeliveryServiceClient -da <customerId> <deliveryClientId> <direction> <postcode>\n"
						+ "    [findDeliveryById]   DeliveryServiceClient -df <deliveryId>\n"
						+ "    [findDelivery]    DeliveryServiceClient -ds <clientId> <status> <index> <count>\n"
						+ "    [updateDeliveryStatus]    DeliveryServiceClient -du <deliveryId> <status>\n"
						+ "    [cancelDelivery]    DeliveryServiceClient -dc <deliveryId>\n");
	}

}

package es.udc.rs.deliveries.client.rest;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBElement;

//import org.glassfish.jersey.jettison.JettisonFeature;









import es.udc.rs.deliveries.client.dto.CustomerDetailsDto;
/*import es.udc.rs.advjaxrstutorial.client.rest.dto.CategoryDtoJaxb;
 import es.udc.rs.advjaxrstutorial.client.rest.dto.InstanceNotFoundExceptionDtoJaxb;
 import es.udc.rs.advjaxrstutorial.client.rest.filter.AuthenticationFilter;
 import es.udc.rs.advjaxrstutorial.client.rest.filter.CheckRequestFilter;
 import es.udc.rs.advjaxrstutorial.client.rest.filter.ErrorLoggerFilter;
 import es.udc.rs.advjaxrstutorial.client.rest.interceptor.GzipDecoder;
 import es.udc.rs.advjaxrstutorial.client.rest.util.CategoryToCategoryDtoJaxbConversor;
 import es.udc.rs.advjaxrstutorial.client.rest.util.JaxbExceptionConversor;
 import es.udc.rs.advjaxrstutorial.client.rest.util.LinkUtil;
 import es.udc.rs.advjaxrstutorial.client.rest.util.CustomerToCustomerDtoJaxbConversor;*/
import es.udc.rs.deliveries.client.dto.CustomerDto;
import es.udc.rs.deliveries.client.dto.DeliveryDetailsDto;
import es.udc.rs.deliveries.client.dto.DeliveryDto;
import es.udc.rs.deliveries.client.dto.DeliveryStatusDto;
import es.udc.rs.deliveries.client.service.ClientDeliveryService;
import es.udc.rs.deliveries.client.service.rest.dto.CustomerDetailsDtoJaxb;
import es.udc.rs.deliveries.client.service.rest.dto.CustomerDtoJaxb;
import es.udc.rs.deliveries.client.service.rest.dto.CustomerDtoJaxbList;
import es.udc.rs.deliveries.client.service.rest.dto.DeliveryDetailsDtoJaxb;
import es.udc.rs.deliveries.client.service.rest.dto.DeliveryDtoJaxb;
import es.udc.rs.deliveries.client.service.rest.dto.DeliveryDtoJaxbList;
import es.udc.rs.deliveries.client.service.rest.dto.InputValidationExceptionDtoJaxb;
import es.udc.rs.deliveries.client.service.rest.dto.InstanceNotCancellableExceptionDtoJaxb;
import es.udc.rs.deliveries.client.service.rest.dto.InstanceNotFoundExceptionDtoJaxb;
import es.udc.rs.deliveries.client.service.rest.dto.InstanceNotRemovableExceptionDtoJaxb;
import es.udc.rs.deliveries.client.service.rest.dto.StatusNotValidExceptionDtoJaxb;
import es.udc.rs.deliveries.client.util.JaxbExceptionConversor;
import es.udc.rs.deliveries.util.exceptions.InstanceNotCancellableException;
import es.udc.rs.deliveries.util.exceptions.InstanceNotRemovableException;
import es.udc.rs.deliveries.util.exceptions.StatusNotValidException;
import es.udc.ws.util.configuration.ConfigurationParametersManager;
import es.udc.ws.util.exceptions.InputValidationException;
import es.udc.ws.util.exceptions.InstanceNotFoundException;

public class RestClientDeliveryService implements ClientDeliveryService {

	private String userName;
	private String password;
	private Client client = null;

	private final static String ENDPOINT_ADDRESS_PARAMETER = "RestClientDeliveryService.endpointAddress";
	private WebTarget endPointWebTarget = null;

	public RestClientDeliveryService() {
		/*
		 * this.userName = userName; this.password = password;
		 */
	}

	private Client getClient() {
		if (client == null) {
			client = ClientBuilder.newClient();
			/*
			 * client.register(new AuthenticationFilter(userName, password));
			 * client.register(CheckRequestFilter.class);
			 * client.register(ErrorLoggerFilter.class);
			 * client.register(GzipDecoder.class);
			 * client.register(JettisonFeature.class);
			 */
		}
		return client;
	}

	private WebTarget getEndpointWebTarget() {
		if (endPointWebTarget == null) {
			endPointWebTarget = getClient().target(
					ConfigurationParametersManager
							.getParameter(ENDPOINT_ADDRESS_PARAMETER));
		}
		return endPointWebTarget;
	}

	@Override
	public MediaType getMediaType() {
		return MediaType.APPLICATION_XML_TYPE;
	}

	@Override
	public Long addCustomer(CustomerDetailsDto customer)
			throws InputValidationException {
		WebTarget wt = getEndpointWebTarget().path("customers");
		Response response = wt
				.request()
				.accept(this.getMediaType())
				.post(Entity.entity(
						new GenericEntity<JAXBElement<CustomerDetailsDtoJaxb>>(
								CustomerDtoToCustomerDtoJaxbConversor
										.toJaxbCustomerDetails(customer)) {
						}, this.getMediaType()));
		try {
			validateResponse(Response.Status.CREATED.getStatusCode(), response);
			return getIdFromHeaders(response);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		} finally {
			if (response != null) {
				response.close();
			}
		}
	}

	@Override
	public CustomerDetailsDto findCustomer(Long customerId)
			throws InstanceNotFoundException {
		WebTarget wt = getEndpointWebTarget().path("customers/{id}")
				.resolveTemplate("id", customerId);
		Response response = wt.request().accept(this.getMediaType()).get();
		try {
			validateResponse(Response.Status.OK.getStatusCode(), response);
			CustomerDetailsDtoJaxb customer = response
					.readEntity(CustomerDetailsDtoJaxb.class);
			return CustomerDtoToCustomerDtoJaxbConversor
					.toCustomerDetailsDto(customer);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		} finally {
			if (response != null) {
				response.close();
			}
		}
	}

	@Override
	public void updateCustomer(CustomerDetailsDto customer)
			throws InputValidationException, InstanceNotFoundException {

		WebTarget wt = getEndpointWebTarget().path("customers/{id}")
				.resolveTemplate("id", customer.getCustomerId());

		Response response = wt
				.request()
				.accept(this.getMediaType())
				.put(Entity.entity(
						new GenericEntity<JAXBElement<CustomerDetailsDtoJaxb>>(
								CustomerDtoToCustomerDtoJaxbConversor
										.toJaxbCustomerDetails(customer)) {
						}, this.getMediaType()));
		try {
			validateResponse(Response.Status.NO_CONTENT.getStatusCode(),
					response);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		} finally {
			if (response != null) {
				response.close();
			}
		}

	}

	public void deleteCustomer(Long customerId)
			throws InstanceNotFoundException, InstanceNotRemovableException {
		WebTarget wt = getEndpointWebTarget().path("customers/{id}")
				.resolveTemplate("id", customerId);
		Response response = wt.request().accept(this.getMediaType()).delete();
		try {
			validateResponse(Response.Status.NO_CONTENT.getStatusCode(),
					response);

		} catch (Exception ex) {
			throw new RuntimeException(ex);
		} finally {
			if (response != null) {
				response.close();
			}
		}
	}

	public List<CustomerDto> findCustomerByKeywords(String keyword,
			String index, String count) {

		WebTarget wt = getEndpointWebTarget().path("customers")
				.queryParam("keyword", keyword).queryParam("startIndex", index)
				.queryParam("count", count);

		Response response = wt.request().accept(MediaType.APPLICATION_XML)
				.get();

		try {
			validateResponse(Response.Status.OK.getStatusCode(), response);
			CustomerDtoJaxbList customers = response
					.readEntity(CustomerDtoJaxbList.class);
			return CustomerDtoToCustomerDtoJaxbConversor
					.toCustomerDtos(customers);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		} finally {
			if (response != null) {
				response.close();
			}
		}

	}

	private void validateResponse(int expectedStatusCode, Response response)
			throws InstanceNotFoundException, InstanceNotCancellableException,
			InputValidationException, InstanceNotRemovableException, StatusNotValidException {
		Response.Status statusCode = Response.Status.fromStatusCode(response
				.getStatus());
		String contentType = response.getMediaType() != null ? response
				.getMediaType().toString() : null;
		boolean expectedContentType = this.getMediaType().toString()
				.equalsIgnoreCase(contentType);
		if (!expectedContentType
				&& statusCode.getStatusCode() > Response.Status.GONE
						.getStatusCode()) {
			throw new RuntimeException("HTTP error; status code = "
					+ statusCode);
		}
		switch (statusCode) {
		case NOT_FOUND: {
			InstanceNotFoundExceptionDtoJaxb exDto = response
					.readEntity(InstanceNotFoundExceptionDtoJaxb.class);
			throw JaxbExceptionConversor.toInstanceNotFoundException(exDto);
		}
		case BAD_REQUEST: {
			InputValidationExceptionDtoJaxb exDto = response
					.readEntity(InputValidationExceptionDtoJaxb.class);
			throw JaxbExceptionConversor.toInputValidationException(exDto);
		}
		case CONFLICT: {
			StatusNotValidExceptionDtoJaxb exDto = response
					.readEntity(StatusNotValidExceptionDtoJaxb.class);
			throw JaxbExceptionConversor.toStatusNotValidException(exDto);
		}
		case GONE: {
			InstanceNotCancellableExceptionDtoJaxb exDto = response
					.readEntity(InstanceNotCancellableExceptionDtoJaxb.class);
			throw JaxbExceptionConversor.toInstanceNotCancellableException(exDto);
		}
		case FORBIDDEN: {
			InstanceNotRemovableExceptionDtoJaxb exDto = response
					.readEntity(InstanceNotRemovableExceptionDtoJaxb.class);
			throw JaxbExceptionConversor.toInstanceNotRemovableException(exDto);
		}
		default:
			if (statusCode.getStatusCode() != expectedStatusCode) {
				throw new RuntimeException("HTTP error; status code = "
						+ statusCode);
			}
			break;
		}
	}

	private static Long getIdFromHeaders(Response response) {
		String location = response.getHeaderString("Location");
		if (location != null) {
			int idx = location.lastIndexOf('/');
			return Long.valueOf(location.substring(idx + 1));
		}
		return null;
	}

	@Override
	public Long addDelivery(DeliveryDetailsDto delivery)
			throws InputValidationException, InstanceNotFoundException {
		WebTarget wt = getEndpointWebTarget().path("deliveries");
		Response response = wt
				.request()
				.accept(this.getMediaType())
				.post(Entity.entity(
						new GenericEntity<JAXBElement<DeliveryDetailsDtoJaxb>>(
								DeliveryDtoToDeliveryDtoJaxbConversor
										.toJaxbDeliveryDetails(delivery)) {
						}, this.getMediaType()));
		try {
			validateResponse(Response.Status.CREATED.getStatusCode(), response);
			return getIdFromHeaders(response);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		} finally {
			if (response != null) {
				response.close();
			}
		}
	}

	@Override
	public DeliveryDetailsDto findDelivery(Long deliveryId)
			throws InstanceNotFoundException {
		WebTarget wt = getEndpointWebTarget().path("deliveries/{id}")
				.resolveTemplate("id", deliveryId);
		Response response = wt.request().accept(this.getMediaType()).get();
		try {
			validateResponse(Response.Status.OK.getStatusCode(), response);
			DeliveryDetailsDtoJaxb delivery = response
					.readEntity(DeliveryDetailsDtoJaxb.class);
			return DeliveryDtoToDeliveryDtoJaxbConversor
					.toDeliveryDetailsDto(delivery);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		} finally {
			if (response != null) {
				response.close();
			}
		}
	}

	@Override
	public void updateDeliveryStatus(Long deliveryId, DeliveryStatusDto status)
			throws InstanceNotFoundException, StatusNotValidException {
		WebTarget wt = getEndpointWebTarget().path("deliveries/{id}/status")
				.resolveTemplate("id", deliveryId)
				.queryParam("status", status);

		Response response = wt.request().accept(MediaType.APPLICATION_XML)
				.post(Entity.entity(null, MediaType.APPLICATION_XML));
		try {
			validateResponse(Response.Status.NO_CONTENT.getStatusCode(),
					response);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		} finally {
			if (response != null) {
				response.close();
			}
		}
	}

	@Override
	public void cancelDelivery(Long deliveryId)
			throws InstanceNotFoundException, InstanceNotCancellableException {
		WebTarget wt = getEndpointWebTarget().path("deliveries/{id}/cancel")
				.resolveTemplate("id", deliveryId);

		Response response = wt.request().accept(MediaType.APPLICATION_XML)
				.post(Entity.entity(null, MediaType.APPLICATION_XML));
		try {
			validateResponse(Response.Status.NO_CONTENT.getStatusCode(),
					response);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		} finally {
			if (response != null) {
				response.close();
			}
		}

	}

	@Override
	public List<DeliveryDto> findDeliveries(Long customerId,
			DeliveryStatusDto status, int index, int count)
			throws InstanceNotFoundException {
		String customerId_r = "";
		String status_r = "";
		if (customerId >= 0) {
			customerId_r = customerId.toString();
		}
		if (!(status == null)) {
			status_r = status.toString();
		}
		WebTarget wt = getEndpointWebTarget().path("deliveries")
				.queryParam("customerId", customerId_r)
				.queryParam("status", status_r).queryParam("index", index)
				.queryParam("count", count);


		Response response = wt.request().accept(MediaType.APPLICATION_XML)
				.get();

		try {
			validateResponse(Response.Status.OK.getStatusCode(), response);
			DeliveryDtoJaxbList deliveries = response
					.readEntity(DeliveryDtoJaxbList.class);
			return DeliveryDtoToDeliveryDtoJaxbConversor
					.toDeliveryDtos(deliveries);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		} finally {
			if (response != null) {
				response.close();
			}
		}
	}

}

package es.udc.rs.deliveries.client.dto;

import java.util.Calendar;

public class DeliveryDto {

	private Long customerId;
	private Long deliveryId;
	private DeliveryStatusDto status;

	public DeliveryDto() {
	}

	public DeliveryDto(Long customerId, Long deliveryId,
			DeliveryStatusDto status) {
		super();
		this.customerId = customerId;
		this.deliveryId = deliveryId;

		this.status = status;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Long getDeliveryId() {
		return deliveryId;
	}

	public void setDeliveryId(Long deliveryId) {
		this.deliveryId = deliveryId;
	}

	public DeliveryStatusDto getStatus() {
		return status;
	}

	public void setStatus(DeliveryStatusDto status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((customerId == null) ? 0 : customerId.hashCode());
		result = prime * result
				+ ((deliveryId == null) ? 0 : deliveryId.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DeliveryDto other = (DeliveryDto) obj;
		if (customerId == null) {
			if (other.customerId != null)
				return false;
		} else if (!customerId.equals(other.customerId))
			return false;
		if (deliveryId == null) {
			if (other.deliveryId != null)
				return false;
		} else if (!deliveryId.equals(other.deliveryId))
			return false;
		if (status != other.status)
			return false;
		return true;
	}

}

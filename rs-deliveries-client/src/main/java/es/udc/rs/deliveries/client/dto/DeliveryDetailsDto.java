package es.udc.rs.deliveries.client.dto;

import java.util.Calendar;

public class DeliveryDetailsDto {

	private Long customerId;
	private Long deliveryId;
	private Long deliveryClientId;
	private Calendar creationDate;
	private Calendar estimatedDate;
	private String direction;
	private int postcode;
	private Calendar deliveredDate;
	private DeliveryStatusDto status;

	public DeliveryDetailsDto() {
	}

	public DeliveryDetailsDto(Long customerId, Long deliveryId,
			Long deliveryClientId, Calendar creationDate,
			Calendar estimatedDate, String direction, int postcode,
			Calendar deliveredDate, DeliveryStatusDto status) {
		super();
		this.customerId = customerId;
		this.deliveryId = deliveryId;
		this.deliveryClientId = deliveryClientId;
		this.creationDate = creationDate;
		this.estimatedDate = estimatedDate;
		this.direction = direction;
		this.postcode = postcode;
		this.deliveredDate = deliveredDate;
		this.status = status;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Long getDeliveryId() {
		return deliveryId;
	}

	public void setDeliveryId(Long deliveryId) {
		this.deliveryId = deliveryId;
	}

	public Long getDeliveryClientId() {
		return deliveryClientId;
	}

	public void setDeliveryClientId(Long deliveryClientId) {
		this.deliveryClientId = deliveryClientId;
	}

	public Calendar getEstimatedDate() {
		return estimatedDate;
	}

	public void setEstimatedDate(Calendar estimatedDate) {
		this.estimatedDate = estimatedDate;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public Integer getPostcode() {
		return postcode;
	}

	public void setPostcode(int postcode) {
		this.postcode = postcode;
	}

	public Calendar getDeliveredDate() {
		return deliveredDate;
	}

	public void setDeliveredDate(Calendar deliveredDate) {
		this.deliveredDate = deliveredDate;
	}

	public DeliveryStatusDto getStatus() {
		return status;
	}

	public void setStatus(DeliveryStatusDto status) {
		this.status = status;
	}

	public Calendar getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result
				+ ((customerId == null) ? 0 : customerId.hashCode());
		result = prime * result
				+ ((deliveredDate == null) ? 0 : deliveredDate.hashCode());
		result = prime
				* result
				+ ((deliveryClientId == null) ? 0 : deliveryClientId.hashCode());
		result = prime * result
				+ ((deliveryId == null) ? 0 : deliveryId.hashCode());
		result = prime * result
				+ ((direction == null) ? 0 : direction.hashCode());
		result = prime * result
				+ ((estimatedDate == null) ? 0 : estimatedDate.hashCode());
		result = prime * result + postcode;
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DeliveryDetailsDto other = (DeliveryDetailsDto) obj;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (customerId == null) {
			if (other.customerId != null)
				return false;
		} else if (!customerId.equals(other.customerId))
			return false;
		if (deliveredDate == null) {
			if (other.deliveredDate != null)
				return false;
		} else if (!deliveredDate.equals(other.deliveredDate))
			return false;
		if (deliveryClientId == null) {
			if (other.deliveryClientId != null)
				return false;
		} else if (!deliveryClientId.equals(other.deliveryClientId))
			return false;
		if (deliveryId == null) {
			if (other.deliveryId != null)
				return false;
		} else if (!deliveryId.equals(other.deliveryId))
			return false;
		if (direction == null) {
			if (other.direction != null)
				return false;
		} else if (!direction.equals(other.direction))
			return false;
		if (estimatedDate == null) {
			if (other.estimatedDate != null)
				return false;
		} else if (!estimatedDate.equals(other.estimatedDate))
			return false;
		if (postcode != other.postcode)
			return false;
		if (status != other.status)
			return false;
		return true;
	}
	
	

}

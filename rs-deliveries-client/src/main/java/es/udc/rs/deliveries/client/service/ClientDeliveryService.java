package es.udc.rs.deliveries.client.service;

import java.util.List;

import javax.ws.rs.core.MediaType;

import es.udc.rs.deliveries.client.dto.CustomerDetailsDto;
import es.udc.rs.deliveries.client.dto.CustomerDto;
import es.udc.rs.deliveries.client.dto.DeliveryDetailsDto;
import es.udc.rs.deliveries.client.dto.DeliveryDto;
import es.udc.rs.deliveries.client.dto.DeliveryStatusDto;
import es.udc.rs.deliveries.util.exceptions.InstanceNotCancellableException;
import es.udc.rs.deliveries.util.exceptions.InstanceNotRemovableException;
import es.udc.rs.deliveries.util.exceptions.StatusNotValidException;
import es.udc.ws.util.exceptions.InputValidationException;
import es.udc.ws.util.exceptions.InstanceNotFoundException;

public interface ClientDeliveryService {

	public Long addCustomer(CustomerDetailsDto customer)
			throws InputValidationException;

	public CustomerDetailsDto findCustomer(Long customerId)
			throws InstanceNotFoundException;

	public List<CustomerDto> findCustomerByKeywords(String keyword,
			String index, String count);

	public void updateCustomer(CustomerDetailsDto customer)
			throws InputValidationException, InstanceNotFoundException;

	public void deleteCustomer(Long customerId)
			throws InstanceNotFoundException, InstanceNotRemovableException;

	public Long addDelivery(DeliveryDetailsDto delivery)
			throws InputValidationException, InstanceNotFoundException;

	public DeliveryDetailsDto findDelivery(Long deliveryId)
			throws InstanceNotFoundException;

	public void updateDeliveryStatus(Long deliveryId, DeliveryStatusDto status)
			throws InstanceNotFoundException, StatusNotValidException;

	public void cancelDelivery(Long deliveryId)
			throws InstanceNotFoundException, InstanceNotCancellableException;

	public List<DeliveryDto> findDeliveries(Long customerId,
			DeliveryStatusDto status, int index, int count)
			throws InstanceNotFoundException;

	public MediaType getMediaType();

}

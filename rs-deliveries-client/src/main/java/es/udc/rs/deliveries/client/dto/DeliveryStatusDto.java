package es.udc.rs.deliveries.client.dto;

public enum DeliveryStatusDto {
	PENDING, SENT, DELIVERED, REJECTED, CANCELLED
}

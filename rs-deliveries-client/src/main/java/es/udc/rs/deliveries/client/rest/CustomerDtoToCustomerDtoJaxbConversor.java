package es.udc.rs.deliveries.client.rest;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;




import es.udc.rs.deliveries.client.dto.CustomerDetailsDto;
import es.udc.rs.deliveries.client.dto.CustomerDto;
import es.udc.rs.deliveries.client.service.rest.dto.CustomerDetailsDtoJaxb;
import es.udc.rs.deliveries.client.service.rest.dto.CustomerDtoJaxb;
import es.udc.rs.deliveries.client.service.rest.dto.CustomerDtoJaxbList;
import es.udc.rs.deliveries.client.service.rest.dto.ObjectFactory;

public class CustomerDtoToCustomerDtoJaxbConversor {

	public static JAXBElement<CustomerDetailsDtoJaxb> toJaxbCustomerDetails(
			CustomerDetailsDto customerDto) {
		CustomerDetailsDtoJaxb customer = new CustomerDetailsDtoJaxb();
		customer.setCustomerId(customerDto.getCustomerId() != null ? customerDto.getCustomerId() : -1);
		customer.setName(customerDto.getName());
		customer.setCif(customerDto.getCif());
		customer.setDirection(customerDto.getDirection());
		customer.setPostcode(customerDto.getPostcode());
		JAXBElement<CustomerDetailsDtoJaxb> jaxbElement= new ObjectFactory().createCustomerDetails(customer);
		return jaxbElement;
	}

	public static CustomerDetailsDto toCustomerDetailsDto(CustomerDetailsDtoJaxb customer) {
		return new CustomerDetailsDto(customer.getCustomerId(), customer.getName(),
				customer.getCif(), customer.getDirection(), customer.getPostcode());
	}

	
	public static JAXBElement<CustomerDtoJaxb> toJaxbCustomer(
			CustomerDto customerDto) {
		CustomerDtoJaxb customer = new CustomerDtoJaxb();
		customer.setCustomerId(customerDto.getCustomerId() != null ? customerDto.getCustomerId() : -1);
		customer.setName(customerDto.getName());
		JAXBElement<CustomerDtoJaxb> jaxbElement= new ObjectFactory().createCustomer(customer);
		return jaxbElement;
	}

	public static CustomerDto toCustomerDto(CustomerDtoJaxb customer) {
		return new CustomerDto(customer.getCustomerId(), customer.getName());
	}

	public static List<CustomerDto> toCustomerDtos(
			CustomerDtoJaxbList customerListDto) {
		List<CustomerDtoJaxb> customerList = customerListDto.getCustomer();
		List<CustomerDto> customerDtos = new ArrayList<>(customerList.size());
		for (int i = 0; i < customerList.size(); i++) {
			customerDtos.add(toCustomerDto(customerList.get(i)));
		}
		return customerDtos;
	}
}

package es.udc.rs.deliveries.client.rest;

import es.udc.rs.deliveries.client.service.ClientDeliveryService;
import es.udc.ws.util.configuration.ConfigurationParametersManager;

public class ClientDeliveryServiceFactory {

    private final static String CLASS_NAME_PARAMETER = 
            "ClientDeliveryServiceFactory.className";
    private static Class<ClientDeliveryService> serviceClass = null;

    private ClientDeliveryServiceFactory() {
    }

    @SuppressWarnings("unchecked")
    private synchronized static Class<ClientDeliveryService> getServiceClass() {

        if (serviceClass == null) {
            try {
                String serviceClassName = ConfigurationParametersManager
                        .getParameter(CLASS_NAME_PARAMETER);
                serviceClass = (Class<ClientDeliveryService>) 
                        Class.forName(serviceClassName);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        return serviceClass;

    }

    public static ClientDeliveryService getService() {

        try {
            return (ClientDeliveryService) getServiceClass().newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }

    }
}

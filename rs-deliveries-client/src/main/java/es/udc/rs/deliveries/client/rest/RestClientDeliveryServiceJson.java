package es.udc.rs.deliveries.client.rest;

import javax.ws.rs.core.MediaType;

public class RestClientDeliveryServiceJson extends RestClientDeliveryService {

	@Override
	public MediaType getMediaType() {
		return MediaType.APPLICATION_JSON_TYPE;
	}

}

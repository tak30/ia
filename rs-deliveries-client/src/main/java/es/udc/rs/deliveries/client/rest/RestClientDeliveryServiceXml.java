package es.udc.rs.deliveries.client.rest;

import javax.ws.rs.core.MediaType;

public class RestClientDeliveryServiceXml extends RestClientDeliveryService {

	@Override
	public MediaType getMediaType() {		
		return MediaType.APPLICATION_XML_TYPE;
	}

}

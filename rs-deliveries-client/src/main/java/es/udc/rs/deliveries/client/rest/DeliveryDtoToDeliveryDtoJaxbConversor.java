package es.udc.rs.deliveries.client.rest;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import es.udc.rs.deliveries.client.dto.DeliveryDetailsDto;
import es.udc.rs.deliveries.client.dto.DeliveryDto;
import es.udc.rs.deliveries.client.dto.DeliveryStatusDto;
import es.udc.rs.deliveries.client.service.rest.dto.DeliveryDetailsDtoJaxb;
import es.udc.rs.deliveries.client.service.rest.dto.DeliveryDtoJaxb;
import es.udc.rs.deliveries.client.service.rest.dto.DeliveryDtoJaxbList;
import es.udc.rs.deliveries.client.service.rest.dto.DeliveryStatusDtoJaxb;
import es.udc.rs.deliveries.client.service.rest.dto.ObjectFactory;

public class DeliveryDtoToDeliveryDtoJaxbConversor {

	public static JAXBElement<DeliveryDetailsDtoJaxb> toJaxbDeliveryDetails(
			DeliveryDetailsDto deliveryDto) {
		DeliveryDetailsDtoJaxb delivery = new DeliveryDetailsDtoJaxb();
		delivery.setDeliveryId(deliveryDto.getDeliveryId() != null ? deliveryDto
				.getDeliveryId() : -1);
		delivery.setCustomerId(deliveryDto.getCustomerId());
		delivery.setDeliveryClientId(deliveryDto.getDeliveryClientId());

		delivery.setDirection(deliveryDto.getDirection());
		delivery.setPostcode(deliveryDto.getPostcode());
		JAXBElement<DeliveryDetailsDtoJaxb> jaxbElement = new ObjectFactory()
				.createDeliveryDetails(delivery);
		return jaxbElement;
	}

	public static DeliveryDetailsDto toDeliveryDetailsDto(
			DeliveryDetailsDtoJaxb delivery) {
		Calendar estimatedDate = Calendar.getInstance();
		estimatedDate.setTimeInMillis(delivery.getEstimatedDate()
				.toGregorianCalendar().getTimeInMillis());
		Calendar creationDate = Calendar.getInstance();
		creationDate.setTimeInMillis(delivery.getCreationDate()
				.toGregorianCalendar().getTimeInMillis());
		Calendar deliveredDate = null;
		if (delivery.getDeliveredDate() != null) {
			deliveredDate = Calendar.getInstance();
			deliveredDate.setTimeInMillis(delivery.getDeliveredDate()
					.toGregorianCalendar().getTimeInMillis());
		}

		DeliveryStatusDto status = DeliveryStatusDto.valueOf(delivery
				.getStatus().value());
		return new DeliveryDetailsDto(delivery.getCustomerId(),
				delivery.getDeliveryId(), delivery.getDeliveryClientId(),
				creationDate, estimatedDate, delivery.getDirection(),
				delivery.getPostcode(), deliveredDate, status);
	}

	public static JAXBElement<DeliveryDtoJaxb> toJaxbDelivery(
			DeliveryDto deliveryDto) {
		DeliveryDtoJaxb delivery = new DeliveryDtoJaxb();
		delivery.setDeliveryId(deliveryDto.getDeliveryId() != null ? deliveryDto
				.getDeliveryId() : -1);
		delivery.setCustomerId(deliveryDto.getCustomerId());
		JAXBElement<DeliveryDtoJaxb> jaxbElement = new ObjectFactory()
				.createDelivery(delivery);
		return jaxbElement;
	}

	public static DeliveryDto toDeliveryDto(DeliveryDtoJaxb delivery) {
		DeliveryStatusDto status = DeliveryStatusDto.valueOf(delivery
				.getStatus().value());
		return new DeliveryDto(delivery.getCustomerId(),
				delivery.getDeliveryId(), status);
	}

	public static List<DeliveryDto> toDeliveryDtos(
			DeliveryDtoJaxbList deliveryListDto) {
		List<DeliveryDtoJaxb> deliveryList = deliveryListDto.getDelivery();
		List<DeliveryDto> deliveryDtos = new ArrayList<>(deliveryList.size());
		for (int i = 0; i < deliveryList.size(); i++) {
			deliveryDtos.add(toDeliveryDto(deliveryList.get(i)));
		}
		return deliveryDtos;
	}

}
